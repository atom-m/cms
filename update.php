<?php

include_once 'sys/boot.php';
$DB = getDB();

$DB->query("ALTER TABLE `" . $DB->getFullTableName("comments") . "`       CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("comments") . "`       CHANGE `editdate` `editdate`     DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("attaches") . "`       CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("messages") . "`       CHANGE `sendtime` `sendtime`     DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("stat") . "`           CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("loads") . "`          CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("foto") . "`           CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("news") . "`           CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("posts") . "`          CHANGE `time` `time`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("posts") . "`          CHANGE `edittime` `edittime`     DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("themes") . "`         CHANGE `time` `time`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("themes") . "`         CHANGE `last_post` `last_post`   DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("users") . "`          CHANGE `puttime` `puttime`       DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("users") . "`          CHANGE `last_visit` `last_visit` DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("users") . "`          CHANGE `ban_expire` `ban_expire` DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("users_votes") . "`    CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("users_warnings") . "` CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");
$DB->query("ALTER TABLE `" . $DB->getFullTableName("search_index") . "`   CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;");

$DB->query("UPDATE `" . $DB->getFullTableName("comments") . "`       SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("comments") . "`       SET `editdate` = NULL   WHERE `editdate` =   '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("attaches") . "`       SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("messages") . "`       SET `sendtime` = NULL   WHERE `sendtime` =   '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("stat") . "`           SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("loads") . "`          SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("foto") . "`           SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("news") . "`           SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("posts") . "`          SET `time` = NULL       WHERE `time` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("posts") . "`          SET `edittime` = NULL   WHERE `edittime` =   '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("themes") . "`         SET `time` = NULL       WHERE `time` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("themes") . "`         SET `last_post` = NULL  WHERE `last_post` =  '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("users") . "`          SET `puttime` = NULL    WHERE `puttime` =    '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("users") . "`          SET `last_visit` = NULL WHERE `last_visit` = '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("users") . "`          SET `ban_expire` = NULL WHERE `ban_expire` = '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("users_votes") . "`    SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("users_warnings") . "` SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");
$DB->query("UPDATE `" . $DB->getFullTableName("search_index") . "`   SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';");

die(__('Operation is successful'));
