<?php
/**
* @project    Atom-M CMS
* @package    Admin Panel module
* @url        https://atom-m.modos189.ru
*/


include_once '../sys/boot.php';
include_once R.'admin/inc/adm_boot.php';

$Register = Register::getInstance();
$DB = getDB();


$pageTitle = __('Admin Panel');


$cnt_entries = 0;
$stat = array();
$installed_modules = \ModuleManager::getInstalledModules();
if (!empty($installed_modules) && is_array($installed_modules)) {
    foreach($installed_modules as $module) {
        $model_name = \OrmManager::getModelName($module);
        if (\Config::read('active', $module) && method_exists($model_name, '__getEntriesCount')) {
            $model = \OrmManager::getModelInstance($module);
            $module_stat = $model->__getEntriesCount();
            if (is_array($module_stat) && count($module_stat) && isset($module_stat['count'])) {
                $stat[] = $module_stat;
                $cnt_entries += $module_stat['count'];
            }
        }
    }
}

$Cache = new \Cache('atmCheckNewVersion');
$Cache->lifeTime = 36000;
if ($Cache->check('atmCheckNewVersion')) {
    $new_ver = $Cache->read('atmCheckNewVersion');
} else {
    $new_ver = @file_get_contents('https://atom-m.modos189.ru/last.php?v=' . ATOM_VERSION);
    $new_ver = ((!empty($new_ver)) && ($new_ver == h($new_ver)) && ($new_ver >= ATOM_VERSION))
        ? '<a class="usually_link" href="https://bitbucket.org/atom-m/cms" title="Last version">' . h($new_ver) . '</a>'
        : '';

    if ($new_ver != '') {
        $Cache->write($new_ver, 'atmCheckNewVersion', array());
    }
}

include 'template/header.php';
?>



<!--************ GENERAL **********-->
<div class="row b15tm">
    <div class="col s12">
        <!--<h5 class="light"><?php echo __('General information') ?></h5>-->
        <table>
            <thead>
                <tr>
                    <th data-field="id"><?php echo __('Current domain'); ?></th>
                    <th data-field="version"><?php echo __('Version of Atom-M'); ?></th>
                    <th data-field="isddos"><?php echo __('Anti DDOS protection'); ?></th>
                    <th data-field="iscache"><?php echo __('Cache'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo (used_https() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/' ?></td>
                    <td>
                        <b><?php echo ATOM_VERSION ?></b> — <a href="<?php echo WWW_ROOT ?>/admin/core_manager.php">управление версиями</a>
                        <?php if ($new_ver): ?>
                            <p>
                            <?php echo __('New version of Atom-M'); ?>
                            <?php echo $new_ver; ?>
                            </p>
                        <?php endif; ?>
                    </td>
                    <td><i class="small <?php echo (\Config::read('anti_ddos', '__secure__') == 1) ? 'mdi-action-done green-text' : 'mdi-image-texture grey-text' ?>"></i></td>
                    <td><i class="small <?php echo (\Config::read('cache') == 1) ? 'mdi-action-done green-text' : 'mdi-image-texture grey-text' ?>"></i></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


<div class="row">

    <!--************ MATERIALS **********-->
    <div class="col s6">
        <script>
        $(document).ready(function () {
            // Build the chart
            $('#materialsPie').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: '<?php echo __('Materials') ?>'
                },
                subtitle: {
                    text: '<?php echo __('All materials') ?>: <?php echo $cnt_entries ?>'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}({point.percentage:.1f}%)</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: "<?php echo __('Materials cnt'); ?>",
                    colorByPoint: true,
                    data: [
                        <?php 
                        $max_index = max(array_keys($stat));
                        foreach ($stat as $index => $module_stat) {
                            echo '{name: "' . $module_stat['text'] . '", y: ' . $module_stat['count'] . '}' . ($index != $max_index ? ',' : '');
                        } ?>
                    ]
                }],
                credits: false
            });
        });
        </script>
        
        <div id="materialsPie"></div>
    </div>



    <?php
    $cards = \Events::init('add_admin_homecard', array());
    
    usort($cards, function ($a, $b) {
        return (int)($a["order"]) > (int)($b["order"]) ? 1 : (int)($a["order"]) == (int)($b["order"]) ? 0 : -1;
    });
    
    foreach($cards as $card) {
        $card = array_merge(array(
            'title' => false,
            'is_row' => false,
            'body' => null,
            'order' => 0
        ), $card);
    ?>
        <?php if (!empty($card['is_row'])) { ?>
    <div class="row">
        <div class="col s12">
        <?php } else { ?>
        <div class="col s6">
        <?php } ?>
            <?php if (!empty($card['title'])) { ?>
            <h5 class="light"><?php echo $card['title'] ?></h5>
            <?php }
            echo $card['body'] ?>
        </div>
        <?php if (!empty($card['is_row'])) { ?>
    </div>
        <?php } ?>
    <?php
    }
    ?>
</div>

<script src="<?php echo WWW_ROOT ?>/admin/js/highcharts.js"></script>

<?php
include_once 'template/footer.php';
?>



