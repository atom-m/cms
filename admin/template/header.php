<!DOCTYPE html>
<?php
    if (\UserAuth::isUser()) {
        $ava_path = getAvatar(\UserAuth::getField('id'), \UserAuth::getField('email'));
    }
    @ini_set('default_socket_timeout', 5);
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        
        <title><?php echo $pageTitle ?></title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="shortcut icon" href="<?php echo WWW_ROOT ?>/favicon.ico" type="image/x-icon">
        
        <!--Import materialize.css-->
        <link rel="stylesheet" href="<?php echo WWW_ROOT ?>/admin/template/css/materialize.min.css"  media="screen,projection"/>
        <link rel="stylesheet" href="<?php echo WWW_ROOT ?>/admin/template/css/main.css"  media="screen,projection"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        
        <script type="text/javascript" src="<?php echo WWW_ROOT ?>/admin/js/jquery.js"></script>
    </head>

    <body>
        <header>
        <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <a href="<?php echo WWW_ROOT ?>/admin" class="brand-logo"><img src="<?php echo WWW_ROOT ?>/admin/template/img/logo.png" alt="Logo Atom-M"/><span class="hide-on-med-and-down brand-text">Admin panel</span></a>
                
                <ul class="right hide-on-med-and-down">
                    <li><a class="dropdown-button waves-effect" href="" data-activates="main_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('General'); ?></a></li>
                    <ul id="main_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__sys__"><?php echo __('Common settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/clean_cache.php"><?php echo __('Clean cache'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="plugins_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Plugins'); ?></a></li>
                    <ul id="plugins_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/plugins.php" style="text-align: center"><?php echo __('Plugins manager'); ?></a></li>
                        <li class="divider"></li>
                        <?php
                            $Cache = new \Cache('pages');
                            $Cache->lifeTime = 6000;
                            if ($Cache->check('adm_pl_settings')) {
                                $list = $Cache->read('adm_pl_settings');
                            } else {
                                $plugins = \PluginManager::getInstalledPlugins();
                                $list = '';
                                if (!empty($plugins) && count($plugins) > 0) {
                                    foreach ($plugins as $plugin) {
                                        if (file_exists(\PluginManager::getConfigFile($plugin))) {
                                            $config = json_decode(file_get_contents(\PluginManager::getConfigFile($plugin)), 1);
                                            if (!empty($config['active'])) {
                                                if (file_exists(\PluginManager::getSettingsFile($plugin))) {
                                                    $list .= "<li><a class=\"waves-effect\" href=\"" . WWW_ROOT . "/admin/plugins.php?ac=edit&dir=$plugin\">" . h($config['title']) . "</a></li>";
                                                }
                                            }
                                        }
                                    }
                                }
                                $Cache->write($list, 'adm_pl_settings', array());
                            }
                            echo $list;
                        ?>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="design_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Design'); ?></a></li>
                    <ul id="design_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/file_manager.php"><?php echo __('File manager'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/menu_editor.php"><?php echo __('Menu editor'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/errors_tmp.php"><?php echo __('Error pages'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="security_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Security'); ?></a></li>
                    <ul id="security_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__secure__"><?php echo __('Security settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/system_log.php"><?php echo __('Action log'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/ip_ban.php"><?php echo __('Bans by IP'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/dump.php"><?php echo __('Backup control'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/rules.php"><?php echo __('Rules editor'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="other_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Additional'); ?></a></li>
                    <ul id="other_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__seo__"><?php echo __('SEO settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__rss__"><?php echo __('RSS settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__sitemap__"><?php echo __('Sitemap settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__preview__"><?php echo __('Preview settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__watermark__"><?php echo __('Watermark settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/additional_fields.php?m=comments"><?php echo __('Add fields comments'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="helps_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Help'); ?></a></li>
                    <ul id="helps_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="https://bitbucket.org/atom-m/cms/wiki/Home" target="_blank"><?php echo __('Wiki'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/faq.php"><?php echo __('FAQ'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/authors.php"><?php echo __('Dev. Team'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="profile_navmenu" data-constrainwidth="false" data-beloworigin="true"><i class="col s2 right"><img src="<?php echo $ava_path; ?>" alt="Avatar" class="circle responsive-img"/></i><?php if (\UserAuth::isUser()) { echo h(\UserAuth::getField('name')); } ?></a></li>
                    <ul id="profile_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/"><?php echo __('Return to site') ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/exit.php"><?php echo __('Sign out') ?></a></li>
                    </ul>
                </ul>
                <!-- For mobile -->
                <a href="#" data-activates="mobile-navmenu" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
                <ul class="side-nav collapsible" data-collapsible="accordion" id="mobile-navmenu">
                    <li class="bold no-padding"><a class="collapsible-header"><?php if (\UserAuth::isUser()) { echo h(\UserAuth::getField('name')); } ?></a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/"><?php echo __('Return to site') ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/exit.php"><?php echo __('Sign out') ?></a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header waves-effect"><?php echo __('General'); ?></a>
                      <div class="collapsible-body transparent">
                        <ul>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__sys__"><?php echo __('Common settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/clean_cache.php"><?php echo __('Clean cache'); ?></a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header"><?php echo __('Plugins'); ?></a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/plugins.php" style="text-align: center"><?php echo __('Plugins manager'); ?></a></li>
                        <li class="divider"></li>
                        <?php
                            $Cache = new \Cache('pages');
                            $Cache->lifeTime = 6000;
                            if ($Cache->check('adm_pl_settings')) {
                                $list = $Cache->read('adm_pl_settings');
                            } else {
                                $plugins = \PluginManager::getInstalledPlugins();
                                $list = '';
                                if (!empty($plugins) && count($plugins) > 0) {
                                    foreach ($plugins as $plugin) {
                                        if (file_exists(\PluginManager::getConfigFile($plugin))) {
                                            $config = json_decode(file_get_contents(\PluginManager::getConfigFile($plugin)), 1);
                                            if (!empty($config['active'])) {
                                                if (file_exists(\PluginManager::getSettingsFile($plugin))) {
                                                    $list .= "<li><a class=\"waves-effect\" href=\"" . WWW_ROOT . "/admin/plugins.php?ac=edit&dir=$plugin\">" . h($config['title']) . "</a></li>";
                                                }
                                            }
                                        }
                                    }
                                }
                                $Cache->write($list, 'adm_pl_settings', array());
                            }
                            echo $list;
                        ?>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header"><?php echo __('Design'); ?></a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/file_manager.php"><?php echo __('File manager'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/menu_editor.php"><?php echo __('Menu editor'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/errors_tmp.php"><?php echo __('Error pages'); ?></a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header"><?php echo __('Security'); ?></a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__secure__"><?php echo __('Security settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/system_log.php"><?php echo __('Action log'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/ip_ban.php"><?php echo __('Bans by IP'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/dump.php"><?php echo __('Backup control'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/rules.php"><?php echo __('Rules editor'); ?></a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header"><?php echo __('Additional'); ?></a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__seo__"><?php echo __('SEO settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__rss__"><?php echo __('RSS settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__sitemap__"><?php echo __('Sitemap settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__preview__"><?php echo __('Preview settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__watermark__"><?php echo __('Watermark settings'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/additional_fields.php?m=comments"><?php echo __('Add fields comments'); ?></a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header"><?php echo __('Help'); ?></a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="https://bitbucket.org/atom-m/cms/wiki/Home" target="_blank"><?php echo __('Wiki'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/faq.php"><?php echo __('FAQ'); ?></a></li>
                        <li class="no-padding"><a href="<?php echo WWW_ROOT ?>/admin/authors.php"><?php echo __('Dev. Team'); ?></a></li>
                        </ul>
                      </div>
                    </li>
                </ul>
            </div>
        </nav>
        </div>
        </header>

        <!-- Page Layout here -->
        <main>
        <div class="row no-margin blue-grey darken-4 flex">
            <div class="col s12 m4 l3 grey-text no-padding"> <!-- Note that "m4 l3" was added -->
            <!-- Grey navigation panel

              This content will be:
            3-columns-wide on large screens,
            4-columns-wide on medium screens,
            12-columns-wide on small screens  -->
                <ul class="main-menu collapsible z-depth-0 b15tm" data-collapsible="expandable">
                    <?php
                    
                    // Неустановленные модули
                    $newmodules = \ModuleManager::getNewModules();

                    if (count($newmodules)):
                        foreach ($newmodules as $modKey):
                    ?>
                        <li>
                            <div class="collapsible-header">
                                <i class="mdi-action-get-app red-text text-accent-1"></i>
                                <?php echo __($modKey,false,$modKey); ?>
                            </div>
                            <div class="collapsible-body">
                                <div class="collection no-margin">
                                    <a class="collection-item" href="<?php echo WWW_ROOT; ?>/admin?install=<?php echo $modKey ?>"><?php echo __('Install') ?></a>
                                </div>
                            </div>
                        </li>
                    <?php
                        endforeach;
                    endif;

                    ?>
                        <li>
                            <div class="collapsible-header waves-effect">
                                <i class="mdi-action-home"></i>
                                <a href="<?php echo WWW_ROOT.'/admin/settings.php?m=__home__'; ?>"><?php echo __('Home'); ?></a>
                            </div>
                        </li>
                    <?php
                    $modules = getAdmFrontMenuParams();
                    
                    // Активные модули
                    $i=0;
                    foreach ($modules as $modKey => $modData): 
                        if (!empty($nsmods) && array_key_exists($modKey, $nsmods)) continue;
                        if (\Config::read('active', $modKey) != 1) continue;
                        $i=1;
                    ?>
                        <li>
                            <div class="collapsible-header waves-effect">
                                <?php if (isset($modData['icon_url'])) {?>
                                    <i><img src="<?php echo $modData['icon_url']; ?>" alt="icon" class="responsive-img"></i>
                                <?php } else { ?>
                                    <i class="<?php echo isset($modData['icon_class']) ? $modData['icon_class'] : 'mdi-action-extension'; ?>"></i>
                                <?php } ?>
                                <?php echo $modData['title']; ?>
                                <i class="mdi-navigation-arrow-drop-down right"></i>
                            </div>
                                <div class="collapsible-body">
                                    <div class="collection no-margin">
                                        <?php if (count($modData['pages']) >= 1) { ?>
                                        <?php foreach ($modData['pages'] as $url => $ankor): ?>
                                            <a class="collection-item waves-effect grey-text" href="<?php echo $url; ?>"><?php echo $ankor; ?></a>
                                        <?php endforeach; ?>
                                        <?php } ?>
                                        <?php if (!in_array($modKey, array('home', 'users'))) { ?>
                                            <a class="collection-item" href="<?php echo WWW_ROOT; ?>/admin?uninstall=<?php echo $modKey ?>"><?php echo __('Uninstall') ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                        </li>
                    <?php endforeach;

                    // Неактивные модули
                    $i=0;
                    foreach ($modules as $modKey => $modData):
                        if (!empty($nsmods) && array_key_exists($modKey, $nsmods)) continue;
                        if (\Config::read('active', $modKey) == 1) continue;
                        $i=1;
                    ?>
                        <li class="grey-text text-darken-2">
                            <div class="collapsible-header waves-effect">
                                <i class="<?php echo isset($modData['icon_class']) ? $modData['icon_class'] : 'mdi-action-extension'; ?>"></i>
                                <?php echo $modData['title']; ?>
                                <i class="mdi-navigation-arrow-drop-down right"></i>
                            </div>
                                <div class="collapsible-body">
                                    <div class="collection no-margin">
                                        <?php if (count($modData['pages']) > 1) { ?>
                                        <?php foreach ($modData['pages'] as $url => $ankor): ?>
                                            <a class="collection-item waves-effect grey-text text-darken-1" href="<?php echo $url; ?>"><?php echo $ankor; ?></a>
                                        <?php endforeach; ?>
                                        <?php } ?>
                                        <?php if (!in_array($modKey, array('home', 'users'))) { ?>
                                            <a class="collection-item" href="<?php echo WWW_ROOT; ?>/admin?uninstall=<?php echo $modKey ?>"><?php echo __('Uninstall') ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="col s12 m8 l9 z-depth-1 flex-1-0-auto grey lighten-4 b30bp"> <!-- Note that "m8 l9" was added -->
            <?php if (isset($pageNav) || isset($pageNavr)) { ?>
                <div class="row b15tm">
                    <div class="col s6">
                        <?php if (isset($pageNav)) echo $pageNav; ?>
                    </div>
                    <div class="col s6 right-align">
                        <?php if (isset($pageNavr)) echo $pageNavr; ?>
                    </div>
                </div>
            <?php } ?>
            <!-- Teal page content

              This content will be:
            9-columns-wide on large screens,
            8-columns-wide on medium screens,
            12-columns-wide on small screens  -->
