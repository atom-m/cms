<?php

/**
 * Created by PhpStorm.
 * User: modos189
 * Date: 09.11.17
 * Time: 16:39
 */

include_once '../sys/boot.php';
include_once './inc/adm_boot.php';

class CoreManagerAdmin
{
    function index() {
        $CM = new \CoreManager;

        $pageTitle = __('Core manager');
        $pageNav = $pageTitle;
        $pageNavr = '';
        include_once ROOT . '/admin/template/header.php';

        $content = '';

        if (!$CM->isPrepare() and !$CM->prepareCore()) {
            $content .= '<blockquote>'.__('Failed to prepare the core').'</blockquote>';
        } else {

            $content .= '
            <ul class="collection with-header">
                <li class="collection-header"><h4>Atom-M CMS</h4></li>';

            foreach ($CM->getCores() as $core => $info) {
                $content .= '<li class="collection-item snapshot ' . $core . ((isset($info['filename'])) ? ' in_repo' : '').'"><div>' . $core;

                $content .= '<a href="#!" class="secondary-content favorite" onclick="selectCore(\'' . $core . '\'); return false" style="'.(($info['installed']) ? "" : "display: none").'"><i class="material-icons">'.(($info['active']) ? "favorite" : "favorite_border").'</i></a>';
                $content .= '<a href="#!" class="secondary-content delete" onclick="deleteCore(\'' . $core . '\'); return false" style="'.(($info['installed'] && !$info['active']) ? "" : "display: none").'"><i class="material-icons">delete</i></a>';
                $content .= '<a href="#!" class="secondary-content file_download" onclick="installCore(\'' . $core . '\', \'' . $info['filename'] . '\'); return false" style="'.((!$info['installed']) ? "" : "display: none").'"><i class="material-icons">file_download</i></a>';

                $content .= '<div class="progress" style="display: none"><div class="indeterminate"></div></div>
                             </div></li>';
            }

            $content .= '</ul>

            <script>
                function selectCore(snapshot) {
                    $.ajax({
                        url: "/admin/core_manager.php",
                        data: "ac=selectCore&core="+snapshot,
                        cache: false,
                        success: function(response){
                            try {
                                response = JSON.parse(response);
                                
                                if (response.status === true) {
                                    $(\'li.snapshot .favorite i\').each(function(){ $(this).text(\'favorite_border\') });
                                    $(\'li.snapshot.\'+snapshot+\' .favorite i\').text(\'favorite\');
                                    Materialize.toast("Успешно сменена версия ядра", 4000);
                                } else {
                                    Materialize.toast("Смена ядра не произведена. Возможно, недостаточно прав в ФС", 4000);
                                }
                                
                            } catch (e) {
                                Materialize.toast("Произошла ошибка при обработке запроса. " +
                                 "Проверьте работоспособность сайта и, при необходимости, " +
                                  "вручную откатите на прошлую версию ядра", 10000);
                            }
                        },
                        error: function(response) {
                            Materialize.toast("Произошла ошибка при обработке запроса. " +
                             "Проверьте работоспособность сайта и, при необходимости, " +
                              "вручную откатите на прошлую версию ядра", 10000);
                        }
                    });
                }
                
                function deleteCore(snapshot) {
                    $.ajax({
                        url: "/admin/core_manager.php",
                        data: "ac=deleteCore&core="+snapshot,
                        cache: false,
                        success: function(response){
                            try {
                                response = JSON.parse(response);
                                
                                if (response.status === true) {
                                    if ($(\'li.snapshot.\'+snapshot).hasClass("in_repo")) {
                                        $(\'li.snapshot.\'+snapshot+\' .file_download\').show(500);
                                        $(\'li.snapshot.\'+snapshot+\' .favorite\').hide(500);
                                        $(\'li.snapshot.\'+snapshot+\' .delete\').hide(500);
                                    } else {
                                        $(\'li.snapshot.\'+snapshot).hide(500);
                                    }
                                    Materialize.toast("Успешно удалено", 4000);
                                } else {
                                    Materialize.toast("Удаление не произведено. Возможно, недостаточно прав в ФС", 4000);
                                }
                                
                            } catch (e) {
                                Materialize.toast("Произошла ошибка при обработке запроса. " +
                                 "Проверьте работоспособность сайта и, при необходимости, " +
                                  "вручную откатите на прошлую версию ядра", 10000);
                            }
                        },
                        error: function(response) {
                            Materialize.toast("Произошла ошибка при обработке запроса. " +
                             "Проверьте работоспособность сайта и, при необходимости, " +
                              "вручную откатите на прошлую версию ядра", 10000);
                        }
                    });
                }   
                
                function installCore(snapshot, filename) {
                    $(\'li.snapshot.\'+snapshot+\' .progress\').show(500);

                    $.ajax({
                        url: "/admin/core_manager.php",
                        data: "ac=installCore&core="+filename,
                        cache: false,
                        success: function(response){
                            $(\'li.snapshot.\'+snapshot+\' .progress\').hide(500);
                            
                            try {
                                response = JSON.parse(response);
                                
                                if (response.status === true) {
                                    $(\'li.snapshot.\'+snapshot+\' .file_download\').hide(500);
                                    $(\'li.snapshot.\'+snapshot+\' .favorite\').show(500);
                                    $(\'li.snapshot.\'+snapshot+\' .delete\').show(500);
                                    Materialize.toast("Успешно установлено", 4000);
                                } else {
                                    Materialize.toast("Смена ядра не произведена. Возможно, недостаточно прав в ФС", 4000);
                                }
                                
                            } catch (e) {
                                Materialize.toast("Произошла ошибка при обработке запроса. " +
                                 "Проверьте работоспособность сайта и, при необходимости, " +
                                  "вручную откатите на прошлую версию ядра", 10000);
                            }
                        },
                        error: function(response) {
                            $(\'li.snapshot.\'+snapshot+\' .progress\').hide(500);
                            
                            Materialize.toast("Произошла ошибка при обработке запроса. " +
                             "Проверьте работоспособность сайта и, при необходимости, " +
                              "вручную откатите на прошлую версию ядра", 10000);
                        }
                    });
                }
            </script>';

        }

        echo $content;
        include_once ROOT . '/admin/template/footer.php';

    }

    function selectCore($name) {
        $CM = new \CoreManager;
        echo json_encode(array('status' => $CM->selectCore($name)));
    }

    function deleteCore($name) {
        $CM = new \CoreManager;
        echo json_encode(array('status' => $CM->deleteCore($name)));
    }

    function installCore($name) {
        $CM = new \CoreManager;
        echo json_encode(array('status' => $CM->installCore($name)));
    }
}

$CoreManagerAdmin = new CoreManagerAdmin();

if (!isset($_GET['ac'])) $_GET['ac'] = 'index';
$actions = array('index', 'selectCore', 'deleteCore', 'downloadCore');

switch ($_GET['ac']) {
    case 'selectCore':
        $CoreManagerAdmin->selectCore($_GET['core']);
        break;
    case 'deleteCore':
        $CoreManagerAdmin->deleteCore($_GET['core']);
        break;
    case 'installCore':
        $CoreManagerAdmin->installCore($_GET['core']);
        break;
    default:
        $CoreManagerAdmin->index();

}
