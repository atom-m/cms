Здесь буду писать все запросы к БД, которые нужно выполнить при обновлении до новой версии движка. При новой установке они выполнятся автоматически.

>>> Atom-M 9
Даты по-умолчанию изменены с '0000-00-00 00:00:00' на NULL, т.к. старую запись не поддерживает MySQL в строгом режиме.

ALTER TABLE `comments`       CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `comments`       CHANGE `editdate` `editdate`     DATETIME NULL DEFAULT NULL;
ALTER TABLE `attaches`       CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `messages`       CHANGE `sendtime` `sendtime`     DATETIME NULL DEFAULT NULL;
ALTER TABLE `stat`           CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `loads`          CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `foto`           CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `news`           CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `posts`          CHANGE `time` `time`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `posts`          CHANGE `edittime` `edittime`     DATETIME NULL DEFAULT NULL;
ALTER TABLE `themes`         CHANGE `time` `time`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `themes`         CHANGE `last_post` `last_post`   DATETIME NULL DEFAULT NULL;
ALTER TABLE `users`          CHANGE `puttime` `puttime`       DATETIME NULL DEFAULT NULL;
ALTER TABLE `users`          CHANGE `last_visit` `last_visit` DATETIME NULL DEFAULT NULL;
ALTER TABLE `users`          CHANGE `ban_expire` `ban_expire` DATETIME NULL DEFAULT NULL;
ALTER TABLE `users_votes`    CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `users_warnings` CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;
ALTER TABLE `search_index`   CHANGE `date` `date`             DATETIME NULL DEFAULT NULL;

UPDATE `comments`       SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `comments`       SET `editdate` = NULL   WHERE `editdate` =   '0000-00-00 00:00:00';
UPDATE `attaches`       SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `messages`       SET `sendtime` = NULL   WHERE `sendtime` =   '0000-00-00 00:00:00';
UPDATE `stat`           SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `loads`          SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `foto`           SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `news`           SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `posts`          SET `time` = NULL       WHERE `time` =       '0000-00-00 00:00:00';
UPDATE `posts`          SET `edittime` = NULL   WHERE `edittime` =   '0000-00-00 00:00:00';
UPDATE `themes`         SET `time` = NULL       WHERE `time` =       '0000-00-00 00:00:00';
UPDATE `themes`         SET `last_post` = NULL  WHERE `last_post` =  '0000-00-00 00:00:00';
UPDATE `users`          SET `puttime` = NULL    WHERE `puttime` =    '0000-00-00 00:00:00';
UPDATE `users`          SET `last_visit` = NULL WHERE `last_visit` = '0000-00-00 00:00:00';
UPDATE `users`          SET `ban_expire` = NULL WHERE `ban_expire` = '0000-00-00 00:00:00';
UPDATE `users_votes`    SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `users_warnings` SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';
UPDATE `search_index`   SET `date` = NULL       WHERE `date` =       '0000-00-00 00:00:00';



>>> Atom-M 7
Файлы ЧПУ перенесены в базу данных. Для подготовки структуры таблиц к изменениям достаточно выполнить запросы:

ALTER TABLE `news` ADD `hlu` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `premoder`;
ALTER TABLE `stat` ADD `hlu` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `premoder`;
ALTER TABLE `loads` ADD `hlu` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `premoder`;

Однако, для обновления существующих записей необходимо выполнить update.php


Поля sourse_email и sourse_site были удалены,
вместо них предлагается использовать только source, либо дополнительные поля.
Чтобы перенести данные из sourse_email и sourse_site в доп.поля
с номерами 10 и 11 соответственно, необходимо выполнить запросы:

ALTER TABLE `news` CHANGE `sourse_email` `add_field_10` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `news` CHANGE `sourse_site` `add_field_11` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `stat` CHANGE `sourse_email` `add_field_10` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `stat` CHANGE `sourse_site` `add_field_11` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `loads` CHANGE `sourse_email` `add_field_10` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `loads` CHANGE `sourse_site` `add_field_11` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;



>>> Atom-M 6
ALTER TABLE `posts` DROP `attaches`;

CREATE TABLE `attaches` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `entity_id` INT NOT NULL,
    `user_id` INT NOT NULL ,
    `attach_number` INT NOT NULL ,
    `filename` VARCHAR( 100 ) NOT NULL ,
    `size` BIGINT NOT NULL ,
    `date` DATETIME NOT NULL ,
    `is_image` ENUM( '0', '1' ) DEFAULT '0' NOT NULL ,
    `module` varchar(10) default 'news' NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO attaches
SELECT '0' as id, post_id as entity_id, user_id, attach_number, filename, size, date, is_image, 'forum' AS module FROM `forum_attaches`
UNION
SELECT '0' as id, entity_id, user_id, attach_number, filename, size, date, is_image, 'loads' AS module FROM `loads_attaches`
UNION
SELECT '0' as id, entity_id, user_id, attach_number, filename, size, date, is_image, 'news' AS module FROM `news_attaches`
UNION
SELECT '0' as id, entity_id, user_id, attach_number, filename, size, date, is_image, 'stat' AS module FROM `stat_attaches`
ORDER BY date ASC;
DROP TABLE IF EXISTS `forum_attaches`;
DROP TABLE IF EXISTS `loads_attaches`;
DROP TABLE IF EXISTS `news_attaches`;
DROP TABLE IF EXISTS `stat_attaches`;


ALTER TABLE `add_fields` ADD `indexed` ENUM( '0', '1' ) DEFAULT '0' NOT NULL;

ALTER TABLE `news` MODIFY `add_field_1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `news` MODIFY `add_field_2` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
...
ALTER TABLE `news` MODIFY `add_field_999` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `stat` MODIFY `add_field_1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `stat` MODIFY `add_field_2` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
...
ALTER TABLE `stat` MODIFY `add_field_999` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `loads` MODIFY `add_field_1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `loads` MODIFY `add_field_2` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
...
ALTER TABLE `loads` MODIFY `add_field_999` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `foto` MODIFY `add_field_1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `foto` MODIFY `add_field_2` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
...
ALTER TABLE `foto` MODIFY `add_field_999` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `users` MODIFY `add_field_1` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `users` MODIFY `add_field_2` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
...
ALTER TABLE `users` MODIFY `add_field_999` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `users` CHANGE `about` `about` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `users` CHANGE `signature` `signature` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


>>> Atom-M 5
ALTER TABLE `statistics`
  DROP `cookie`,
  DROP `referer`,
  DROP `yandex_bot_views`,
  DROP `google_bot_views`,
  DROP `other_bot_views`;

ALTER TABLE `statistics` CHANGE  `ips`  `visits` INT( 50 ) NULL DEFAULT  1;
ALTER TABLE `statistics` ADD  `bot_views` BLOB NOT NULL;


>>> Atom-M 4
ALTER TABLE `news` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;
ALTER TABLE `stat` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;
ALTER TABLE `loads` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;
ALTER TABLE `foto` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;

ALTER TABLE `comments` ADD `parent_id` INT(11) NULL DEFAULT '0' AFTER `id`;

ALTER TABLE `posts` ADD `id_forum` INT(11) NULL DEFAULT '0' AFTER `id_theme`;
UPDATE  `posts` SET `id_forum` = (SELECT `id_forum` FROM `themes` WHERE `id` = `posts`.`id_theme`);

DROP TABLE IF EXISTS `news_add_fields`;
DROP TABLE IF EXISTS `loads_add_fields`;
DROP TABLE IF EXISTS `stat_add_fields`;
DROP TABLE IF EXISTS `users_add_fields`;
DROP TABLE IF EXISTS `news_add_content`;
DROP TABLE IF EXISTS `loads_add_content`;
DROP TABLE IF EXISTS `stat_add_content`;
DROP TABLE IF EXISTS `users_add_content`;
CREATE TABLE `add_fields` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `field_id` INT(11) NOT NULL,
    `module` VARCHAR(100) NOT NULL,
    `type` VARCHAR(10) NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `size` INT(11) NOT NULL,
    `params` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;

RENAME TABLE `news_sections` TO `news_categories`;
RENAME TABLE `foto_sections` TO `foto_categories`;
RENAME TABLE `loads_sections` TO `loads_categories`;
RENAME TABLE `stat_sections` TO `stat_categories`;



>>> 31 марта 2014
ALTER TABLE `comments` ADD `premoder` enum('nochecked','rejected','confirmed') NOT NULL DEFAULT 'nochecked';



>>> 30 июля 2013
ALTER TABLE `users` ADD `full_name` VARCHAR( 100 ) CHARACTER SET utf8 NOT NULL AFTER `name`;



>>> 22 июня 2013
ALTER TABLE `news` ADD `premoder` ENUM( 'nochecked', 'rejected', 'confirmed' ) NOT NULL DEFAULT 'confirmed';
ALTER TABLE `stat` ADD `premoder` ENUM( 'nochecked', 'rejected', 'confirmed' ) NOT NULL DEFAULT 'confirmed';
ALTER TABLE `loads` ADD `premoder` ENUM( 'nochecked', 'rejected', 'confirmed' ) NOT NULL DEFAULT 'confirmed';
ALTER TABLE `pages` ADD `meta_title` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `url`;
ALTER TABLE `pages` ADD `title` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `name`;
ALTER TABLE `pages` ADD `publish` ENUM( '0', '1' ) NOT NULL DEFAULT '1';
ALTER TABLE `pages` ADD `position` INT( 11 ) NOT NULL AFTER `visible`;


>>> May 14, 2013
CREATE TABLE `comments` (
  `id` int(11) NOT NULL auto_increment,
  `entity_id` int(11) NOT NULL,
  `user_id` INT(11) DEFAULT '0' NOT NULL,
  `name` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `ip` varchar(50) NOT NULL,
  `mail` varchar(150) NOT NULL,
  `date` DATETIME NOT NULL,
  `editdate` DATETIME NOT NULL,
  `module` varchar(10) default 'news' NOT NULL, 
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
INSERT INTO comments
SELECT '0' as id, entity_id, user_id, name, message, ip, mail, date, editdate, 'foto' AS module FROM `foto_comments`
UNION
SELECT '0' as id, entity_id, user_id, name, message, ip, mail, date, editdate, 'loads' AS module FROM `loads_comments`
UNION
SELECT '0' as id, entity_id, user_id, name, message, ip, mail, date, editdate, 'news' AS module FROM `news_comments`
UNION
SELECT '0' as id, entity_id, user_id, name, message, ip, mail, date, editdate, 'stat' AS module FROM `stat_comments`
ORDER BY date ASC;
DROP TABLE IF EXISTS `foto_comments`;
DROP TABLE IF EXISTS `loads_comments`;
DROP TABLE IF EXISTS `news_comments`;
DROP TABLE IF EXISTS `stat_comments`;



>>> Feb 10, 2013
ALTER TABLE `themes` ADD `first_top` ENUM( '0', '1' ) DEFAULT '0' NOT NULL



>>> Jan 28, 2013
CREATE TABLE `polls` (`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`theme_id` int( 11 ) NOT NULL ,
`question` text NOT NULL ,
`variants` text NOT NULL ,
`voted_users` text NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM DEFAULT CHARSET = utf8;



>>> Jan 08, 2013
ALTER TABLE `users` ADD `template` VARCHAR( 255 ) NOT NULL




>>> Jan 03, 2013
ALTER TABLE `news_comments` ADD `editdate` DATETIME NOT NULL
ALTER TABLE `stat_comments` ADD `editdate` DATETIME NOT NULL
ALTER TABLE `loads_comments` ADD `editdate` DATETIME NOT NULL
ALTER TABLE `foto_comments` ADD `editdate` DATETIME NOT NULL

ALTER TABLE `loads` ADD `filename` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `download`




>>> Oct 06, 2012
ALTER TABLE `foto` ADD `commented` ENUM( '0', '1' ) NOT NULL DEFAULT '1'

CREATE TABLE `foto_comments` (`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`entity_id` int( 11 ) NOT NULL ,
`user_id` int( 11 ) NOT NULL DEFAULT '0',
`name` varchar( 100 ) NOT NULL ,
`message` text NOT NULL ,
`ip` varchar( 50 ) NOT NULL ,
`mail` varchar( 150 ) NOT NULL ,
`date` datetime NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM DEFAULT CHARSET = utf8;
