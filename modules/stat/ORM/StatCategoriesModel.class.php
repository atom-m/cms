<?php
/**
* @project    Atom-M CMS
* @package    Stat Sections Model
* @url        https://atom-m.modos189.ru
*/


namespace StatModule\ORM;

class StatCategoriesModel extends \OrmModel {
    public $Table = 'stat_categories';
}