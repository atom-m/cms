<?php
/**
* @project    Atom-M CMS
* @package    Pages Model
* @url        https://atom-m.modos189.ru
*/


namespace HomeModule\ORM;

class HomeModel extends \OrmModel
{
    public function __construct()
    {
        parent::__construct();
    }


    public function getEntitiesByHomePage($latest_on_home) {
        $Register = \Register::getInstance();
        $models = array();
        $materials = array();
        $entities = array();
        $sql = '';

        foreach ($latest_on_home as $module) {
            $model_name = \OrmManager::getModelName($module);
            if (method_exists($model_name, '__getQueryForHome')) {
                $model = \OrmManager::getModelInstance($module);
                $model->bindModel('categories');
                $model->bindModel('author');
                $related = $model->getRelatedEntitiesParams();
                if (isset($related['attaches'])) {
                    $model->bindModel('attaches', array(), array('module' => $module));
                }
                $models[$module] = $model;
                $sql .= (!empty($sql) ? 'UNION ' : '') . $model->__getQueryForHome();
                $entities[$module] = array();
            }
        }

        if (!empty($sql)) {
            $sql .= 'ORDER BY `on_home_top` DESC, `date` DESC LIMIT ' . $Register['Config']->read('cnt_latest_on_home');
            $materials = getDB()->query($sql);
            if ($materials && is_array($materials)) {
                foreach ($materials as $key => $material) {
                    $entities[$material['skey']][$key] = $material;
                }
                $materials = array();
                foreach ($entities as $module => $module_materials) {
                    if (\ModuleManager::checkInstallModule($module) && isset($models[$module])) {
                        $module_materials = $models[$module]->getAllAssigned($module_materials);
                        if (is_array($module_materials) && count($module_materials)) {
                            $entity = \OrmManager::getEntityName($module);
                            foreach ($module_materials as $key => $material) {
                                $materials[$key] = new $entity($material);
                            }
                        }
                    }
                }
                ksort($materials);
            }
        }
        return $materials;
    }

}