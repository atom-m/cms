<?php
/**
* @project    Atom-M CMS
* @package    Home Module
* @url        https://atom-m.modos189.ru
*/

namespace HomeModule;

Class ActionsHandler extends \Module {

    /**
     * @module_title  title of module
     */
    public $module_title = 'Главная';

    /**
     * @template  layout for module
     */
    public $template = 'default';

    /**
     * @module module indentifier
     */
    public $module = 'home';


    function __construct($params) {
        parent::__construct($params);

        $this->setModel();
        
        
    }

    /**
     * default action
     */
    function index() {
        $this->page_title = \Config::read('title');
        $latest_on_home = \Config::read('latest_on_home');
        foreach ($latest_on_home as $index => $module) {
            if (!\ModuleManager::checkInstallModule($module) || !\Config::read('active', $module) ||
                    !\ACL::turnUser(array($module, 'view_list')) || !\ACL::turnUser(array($module, 'view_materials'))) {
                unset($latest_on_home[$index]);
            }
        }
        $navi = null; //vsyakiy sluchay:)
        //if we want view latest materials on home page
        if (is_array($latest_on_home) && count($latest_on_home) > 0) {

            // Navigation Block
            $navi = array();
            $navi['add_link'] = (\ModuleManager::checkInstallModule('news') && \Config::read('active', 'news') && \ACL::turnUser(array('news','add_materials'))) ? get_link(__('Add material'), '/news/add/') : '';
            $navi['navigation'] = $this->buildBreadCrumbs();
            $this->_globalize($navi);


            // Reading from cache
            if ($this->cached && $this->Cache->check($this->cacheKey)) {
                $html = $this->Cache->read($this->cacheKey);
                return $this->_view($html);
            }


            // Create SQL query
            $entities = $this->Model->getEntitiesByHomePage($latest_on_home);

            // If we have records
            if (count($entities) > 0) {
                foreach ($entities as $result) {
                    // Create and replace markers
                    $this->Register['current_vars'] = $result;

                    $entry_url = get_url(entryUrl($result, $result->getSkey()));
                    $result->setEntry_url($entry_url);


                    $matattaches = ($result->getAttaches() && count($result->getAttaches())) ? $result->getAttaches() : array();
                    $announce = $result->getMain();


                    $announce = \PrintText::getAnnounce($announce, '', \Config::read('announce_length'), $result);


                    if (count($matattaches) > 0) {
                        $img = array();
                        foreach ($matattaches as $attach) {
                            if ($attach->getIs_image() == '1') {
                                $announce = $this->insertImageAttach($announce, $attach->getFilename(), $attach->getAttach_number(), $result->getSkey());
                                $img['url_'.$attach->getAttach_number()] = $this->markerImageAttach($attach->getFilename(), $attach->getAttach_number(),  $result->getSkey());
                                $img['small_url_'.$attach->getAttach_number()] = $this->markerSmallImageAttach($attach->getFilename(), $attach->getAttach_number(), $result->getSkey());
                            }
                        }
                        $result->setImg($img);
                    }

                    $result->setAnnounce($announce);

                    $result->setProfile_url(getProfileUrl($result->getAuthor_id()));

                    $result->setModule_name($result->getSkey());

                    $categories = $result->getCategories();
                    foreach ($categories as $category) {
                        $category->setUrl(get_url($result->getSkey() . '/category/' . $category->getId()));
                    }
                    // New marker contains array with all categories
                    $result->setCategories($categories);

                    // Old markers are based on first category
                    $result->setCategory($categories[0]);
                    $result->setCategory_name($categories[0]->getTitle());
                    $result->setCategory_url($categories[0]->getUrl());

                    $result->setModule_title(\Config::read('title', $result->getSkey()));


                    // Set users_id that are on this page
                    $this->setCacheTag(array(
                        'module_' . $result->getSkey(),
                        'record_id_' . $result->getId(),
                    ));
                }

                $html = $this->render('list.html.twig', array('entities' => $entities));
            } else {
                $html = $this->render('list.html.twig');
            }
            
            // Writing to cache
            if ($this->cached) {
                $this->Cache->write($html, $this->cacheKey, $this->cacheTags);
            }
            
            return $this->_view($html);
        } else {
            $html = $this->render('list.html.twig');
            return $this->_view($html);
        }
        return $this->showMessage(__('Some error occurred'));
    }
}
