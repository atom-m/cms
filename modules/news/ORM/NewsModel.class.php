<?php
/**
* @project    Atom-M CMS
* @package    News Model
* @url        https://atom-m.modos189.ru
*/


namespace NewsModule\ORM;

class NewsModel extends \OrmModel
{
    public $Table = 'news';

    protected $RelatedEntities = array(
        'author' => array(
            'model' => 'Users',
            'type' => 'has_one',
            'foreignKey' => 'author_id',
        ),
        'category' => array( // Deprecated, because not supporting multiple categories
            'model' => 'NewsCategories',
            'type' => 'has_one',
            'foreignKey' => 'category_id',
        ),
        'categories' => array(
            'model' => 'NewsCategories',
            'type' => 'has_many',
            'foreignKey' => 'this.category_id',
        ),
        'attaches' => array(
            'model' => 'Attaches',
            'type' => 'has_many',
            'foreignKey' => 'entity_id',
        ),
    );

    public function getIdByHLU($title, $year, $month, $day = null)
    {
        $entities = getDB()->select($this->Table, DB_FIRST, array(
            'cond' => array(
                'hlu' => $title,
                'YEAR(`date`) = "'.intval($year).'"',
                'MONTH(`date`) = "'.intval($month).'"',
                (($day) ? 'DAY(`date`) = "'.intval($day).'"' : '')
            )
        ));

        if ($entities && count($entities)) {
            return (isset($entities[0]['id'])) ? intval($entities[0]['id']) : false;
        }

        return false;
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    function __getUserStatistic($user_id) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        
        $user_id = intval($user_id);
        if ($user_id > 0) {
            $result = $this->getTotal(array('cond' => array('author_id' => $user_id)));
            if ($result) {
                $res = array(
                    'module' => $module,
                    'text' => __('news',true,$module),
                    'count' => intval($result),
                    'url' => get_url("/$module/user/$user_id"),
                );

                return array($res);
            }
        }
        return false;
    }

    /**
     * Count of entries
     */
    function __getEntriesCount() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        return array(
            'text' => __('news',true,$module),
            'count' => $this->getTotal(),
        );
    }
    
    /**
     * Entries for Homepage
     *
     */
    function __getQueryForHome() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        return "(SELECT `title`, `main`, `date`, `on_home_top`, `id`, `views`, `author_id`, `category_id`, `comments`, `view_on_home`, `hlu`, (SELECT \"$module\") AS skey  FROM `"
            . getDB()->getFullTableName($this->Table) . "` "
            . "WHERE `view_on_home` = '1' AND `available` = '1' AND `premoder` = 'confirmed') ";
    }
    
    /**
     * Entries for search
     *
     */
    function __getQueriesForSearch() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        return array(
            "SELECT `main` as `index`, `id` as `entity_id`, '$this->Table' as `entity_table`, '/view/' as `entity_view`, '$module' as `module`, `date` FROM `" .  getDB()->getFullTableName($this->Table) . "` WHERE `main` IS NOT NULL AND `main` <> ''",
            "SELECT `title` as `index`, `id` as `entity_id`, '$this->Table' as `entity_table`, '/view/' as `entity_view`, '$module' as `module`, `date` FROM `" .  getDB()->getFullTableName($this->Table) . "` WHERE `title` IS NOT NULL AND `title` <> ''",
            "SELECT `tags` as `index`, `id` as `entity_id`, '$this->Table' as `entity_table`, '/view/' as `entity_view`, '$module' as `module`, `date` FROM `" .  getDB()->getFullTableName($this->Table) . "` WHERE `tags` IS NOT NULL AND `tags` <> ''",
        );
    }
    
    /**
     * Results for search
     *
     */
   /**
     * Results for search
     *
     */
    function __correctSearchResults($results) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        
        $comments_id = array();
        $entities_id = array();
        
        $need_comments = \ACL::turnUser(array($module, 'view_comments'));
        $need_entities = \ACL::turnUser(array($module, 'view_materials'));
        if (!$need_comments && !$need_entities) {
            return array();
        }
        foreach ($results as $result) {
            if ($result->getEntity_table() == 'comments' && $need_comments) { // Comment
                $comments_id[] = $result->getEntity_id();
            } elseif ($need_entities) { // Title, text or tags material
                $entities_id[] = $result->getEntity_id();
            }
        }
        $comments_id = array_unique($comments_id);
        $entities_id = array_unique($entities_id);
        
        $this->bindModel('attaches', array(), array('module' => $module));
        $this->bindModel('categories');
        $items = count($comments_id) || count($entities_id) ? $this->getCollection(array(
            (count($entities_id) ? '`id` IN (' . implode(',', $entities_id) . ')' : '')
            . (count($comments_id) && count($entities_id) ? ' OR ' : '')
            . (count($comments_id) ? '`id` IN (SELECT `entity_id` FROM ' . getDB()->getFullTableName('comments') . ' WHERE `id` IN (' . implode(',', $comments_id) . '))' : '')
        )) : array();
        $entities = array();
        foreach ($items as $entity) {
            $entities[$entity->getId()] = $entity;
        }
        
        $commentsModel = \OrmManager::getModelInstance('comments');
        $items = count($comments_id) ? $commentsModel->getCollection(array('`id` in (' . implode(',', $comments_id) . ')')) : array();
        $comments = array();
        foreach ($items as $comment) {
            $comments[$comment->getId()] = $comment;
        }
        
        $usersModel = \OrmManager::getModelInstance('users');
        $items = count($comments_id) || count($entities_id) ? $usersModel->getCollection(array(
            (count($entities_id) ? '`id` IN (SELECT `author_id` FROM ' . getDB()->getFullTableName($this->Table) . ' WHERE `id` IN (' . implode(',', $entities_id) . '))' : '')
            . (count($comments_id) && count($entities_id) ? ' OR ' : '')
            . (count($comments_id) ? '`id` IN (SELECT `user_id` FROM ' . getDB()->getFullTableName('comments') . ' WHERE `id` IN (' . implode(',', $comments_id) . '))' : '')
        )) : array();
        $users = array();
        foreach ($items as $user) {
            $users[$user->getId()] = $user;
        }
        
        foreach ($results as $index => &$result) {
            if (($result->getEntity_table() == 'comments' && !$need_comments) ||
                ($result->getEntity_table() != 'comments' && !$need_entities)) {
                unset($results[$key]);
            }
            $result->setEntry_url(get_url($result->getModule() . $result->getEntity_view() . $result->getEntity_id()));
            $entity = isset($entities[$result->getEntity_id()]) ? $entities[$result->getEntity_id()] : null;
            if ($entity) {
                $result->setAuthor(isset($users[$entity->getAuthor_id()]) ? $users[$entity->getAuthor_id()] : null);
                if ($result->getEntity_table() == 'comments') {
                    $comment = isset($comments[$result->getEntity_id()]) ? $comments[$result->getEntity_id()] : null;
                    $result->setAuthor($comment && isset($users[$comment->getUser_id()]) ? $users[$comment->getUser_id()] : null);
                }
                $entity_categories = $entity->getCategories();
                if (count($entity_categories)) {
                    foreach ($entity_categories as $entity_category) {
                        $entity_category->setUrl(get_url($result->getModule() . '/category/' . $entity_category->getId()));
                    }

                    // New marker contains array with all categories
                    $result->setCategories($entity_categories);

                    // Old markers are based on first category
                    $entity->setCategory_title($entity_categories[0]->getTitle());
                    $entity->setCategory_url($entity_categories[0]->getUrl());
                }

                if ($result->getEntity_table() != 'comments') {
                    $result->setAttaches($entity->getAttaches());
                }
                if ($entity->getTags()) {
                    $result->setTags(atrim(explode(',', $entity->getTags())));
                }

                $result->setTitle($entity->getTitle());
                $result->setViews($entity->getViews());
                $result->setComments($entity->getComments());
            }
        }
        return $results;
    }
    
    /**
     * List of links for Sitemap
     *
     */
    function __getSitemapList($host) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        $urls = array();
        $AtmUrl = \Register::getClass('AtmUrl');

        $entities = getDB()->select($this->Table, DB_ALL, array());
        if (count($entities) > 0) {
            foreach ($entities as $entity) {
                $unixtime = strtotime($entity['date']);

                $year = date('Y', $unixtime);
                $month = date('m', $unixtime);
                $day = date('d', $unixtime);

                $url = $AtmUrl->getEntryUrl($entity['id'], $module, $entity['hlu'], $year, $month, $day);

                $urls[] = $host . $url;
            }
        }
        return $urls;
    }
}
