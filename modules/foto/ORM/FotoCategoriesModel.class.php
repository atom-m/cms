<?php
/**
* @project    Atom-M CMS
* @package    Foto Sections Model
* @url        https://atom-m.modos189.ru
*/


namespace FotoModule\ORM;

class FotoCategoriesModel extends \OrmModel {
    public $Table = 'foto_categories';
}