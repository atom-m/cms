<?php

/**
 * @project    Atom-M CMS
 * @package    Foto Module
 * @url        https://atom-m.modos189.ru
 */

namespace FotoModule;

Class ActionsHandler extends \Module {

    use \Traits\Rss,
        \Traits\Categories,
        \Traits\Comments;

    /**
     * @module_title  title of module
     */
    public $module_title = 'Фото';

    /**
     * @template  layout for module
     */
    public $template = 'foto';

    /**
     * @module module indentifier
     */
    public $module = 'foto';

    public function __construct($params) {
        parent::__construct($params);

        $this->setModel();
    }

    /**
     * default action ( show main page )
     */
    public function index() {
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);

        //формируем блок со списком  разделов
        $this->buildCategoriesMarker();

        // Reading from cache
        if ($this->cached && $this->Cache->check($this->cacheKey)) {
            $html = $this->Cache->read($this->cacheKey);
            return $this->_view($html);
        }

        $where = filter();

        $total = $this->Model->getTotal(array('cond' => $where));
        $perPage = intval(\Config::read('per_page', $this->module));
        if ($perPage < 1) {
            $perPage = 10;
        }
        list ($pages, $page) = pagination($total, $perPage, $this->getModuleURL());
        $this->Register['pages'] = $pages;
        $this->Register['page'] = $page;
        //$this->page_title .= ' (' . $page . ')';

        $navi = array();
        $navi['add_link'] = (\ACL::turnUser(array($this->module, 'add_materials'))) ? get_link(__('Add material'), $this->getModuleURL('add/')) : '';
        $navi['navigation'] = $this->buildBreadCrumbs();
        $navi['pagination'] = $pages;

        $cntPages = ceil($total / $perPage);
        $recOnPage = ($page == $cntPages) ? ($total % $perPage) : $perPage;
        $firstOnPage = ($page - 1) * $perPage + 1;
        $lastOnPage = $firstOnPage + $recOnPage - 1;

        $navi['meta'] = __('Count all material') . ' ' . $total . '. ' . ($total > 1 ? __('Count visible') . ' ' . $firstOnPage . '-' . $lastOnPage : '');
        $this->_globalize($navi);

        if ($total <= 0) {
            $html = $this->render('list.html.twig');
            return $this->_view($html);
        }

        $order = getOrderParam($this->module);
        $params = array(
            'page' => $page,
            'limit' => $perPage,
            'order' => (empty($order) ? 'date DESC' : $order),
        );

        $this->Model->bindModel('author');
        $this->Model->bindModel('categories');
        $records = $this->Model->getCollection($where, $params);

        if (count($records) > 0) {
            $records = $this->mergeAdditionalFields($records);
        }

        $html = $this->_renderList($records);

        // Writing to cache
        if ($this->cached) {
            $this->Cache->write($html, $this->cacheKey, $this->cacheTags);
        }

        return $this->_view($html);
    }

    /**
     * Render entities.
     */
    private function _renderList($records) {
        if (\Config::read('use_local_preview', $this->module)) {
            $preview_size_x = \Config::read('img_size_x', $this->module);
            $preview_size_y = \Config::read('img_size_y', $this->module);
        } else {
            $preview_size_x = \Config::read('img_size_x');
            $preview_size_y = \Config::read('img_size_y');
        }

        // create markers
        foreach ($records as $entity) {
            $this->Register['current_vars'] = $entity;

            $entry_url = get_url(entryUrl($entity, $this->module));
            $entity->setEntry_url($entry_url);
            $entity->setPreview_foto(WWW_ROOT . $this->getImagesPath($entity->getFilename(), null, $preview_size_x, $preview_size_y));
            $entity->setFull_foto(WWW_ROOT . $this->getImagesPath($entity->getFilename()));
            $entity->setFoto_alt(h(preg_replace('#[^\w\d ]+#ui', ' ', $entity->getTitle())));

            // Категории
            $entity_categories = $entity->getCategories();
            foreach ($entity_categories as $entity_category) {
                $entity_category->setUrl(get_url($this->getModuleURL('category/' . $entity_category->getId())));
            }
            // New marker contains array with all categories
            $entity->setCategories($entity_categories);

            // Old markers are based on first category
            $entity->setCategory($entity_categories[0]);
            $entity->setCategory_name($entity_categories[0]->getTitle());
            $entity->setCategory_url($entity_categories[0]->getUrl());

            $entity->setProfile_url(getProfileUrl($entity->getAuthor_id()));

            // Set cache tags
            $this->setCacheTag(array(
                'user_id_' . $entity->getAuthor_id(),
                'record_id_' . $entity->getId(),
            ));
        }

        $html = $this->render('list.html.twig', array('entities' => $records));

        return $html;
    }

    /**
     * Show materials in category. Category ID must be integer and not null.
     */
    public function category($id = null) {
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);

        // \Validate
        if (!empty($id)) {
            $id_ = explode(',', $id);
            foreach ($id_ as $v) {
                if (!is_numeric($v)) {
                    return $this->showMessage(__('Value must be numeric'));
                }
            }
            if (($c = count($id_)) > 1 && count(array_unique($id_)) !== $c) {
                return $this->showMessage(__('Some error occurred'));
            }
        }

        if (empty($id) || $id < 1) {
            return $this->showMessage(__('Can not find category'));
        }

        $categories = $this->getCategoriesByIds($id_);
        if (!$categories) {
            return $this->showMessage(__('Can not find category'));
        }

        $cat_title = array();
        foreach ($categories as $category) {
            if ($category == false || !is_object($category)) {
                return $this->showMessage(__('Can not find category'));
            }
            if (!\ACL::checkAccessInList($category->getNo_access())) {
                $cat_title[] = $category->getTitle();
            }
        }

        if (empty($cat_title)) {
            return $this->showMessage(__('Permission denied'));
        }

        $cat_title = implode(', ', $cat_title);

        $this->page_title = h($cat_title);

        //формируем блок со списком  разделов
        $this->buildCategoriesMarker($id);

        // Reading from cache
        if ($this->cached && $this->Cache->check($this->cacheKey)) {
            $html = $this->Cache->read($this->cacheKey);
            return $this->_view($html);
        }

        // Выборка материалов в указанных категориях
        $query = '';
        if (count($id_) > 0) {
            foreach ($id_ as $n => $catid) {
                if ($n > 0) {
                    $query .= " OR ";
                }
                $query .= "LOCATE('," . $catid . ",',CONCAT(',',`category_id`,',')) > 0";
            }
            // Увеличиваем множество дочерними категориями.
            $childCats = $this->getChildrenCategories($id_);
            if ($childCats && is_array($childCats) && count($childCats) > 0) {
                foreach ($childCats as $pcatid) {
                    $query .= " OR LOCATE('," . $pcatid . ",',CONCAT(',',`category_id`,',')) > 0";
                }
            }
        }
        $where = filter(array($query));

        $total = $this->Model->getTotal(array('cond' => $where));
        $perPage = intval(\Config::read('per_page', $this->module));
        if ($perPage < 1) {
            $perPage = 10;
        }
        list ($pages, $page) = pagination($total, $perPage, $this->getModuleURL('category/' . $id));
        $this->Register['pages'] = $pages;
        $this->Register['page'] = $page;
        //$this->page_title .= ' (' . $page . ')';

        $navi = array();
        $navi['add_link'] = (\ACL::turnUser(array($this->module, 'add_materials'))) ? get_link(__('Add material'), $this->getModuleURL('add/')) : '';

        $navi['navigation'] = $this->buildBreadCrumbs($id);

        $navi['pagination'] = $pages;

        $cntPages = ceil($total / $perPage);
        $recOnPage = ($page == $cntPages) ? ($total % $perPage) : $perPage;
        $firstOnPage = ($page - 1) * $perPage + 1;
        $lastOnPage = $firstOnPage + $recOnPage - 1;

        $navi['meta'] = __('Count material in cat') . ' ' . $total . '. ' . ($total > 1 ? __('Count visible') . ' ' . $firstOnPage . '-' . $lastOnPage : '');
        $navi['category_name'] = h($cat_title);
        $this->_globalize($navi);

        if ($total <= 0) {
            $html = $this->render('list.html.twig');
            return $this->_view($html);
        }

        $order = getOrderParam($this->module);
        $params = array(
            'page' => $page,
            'limit' => $perPage,
            'order' => (empty($order) ? 'date DESC' : $order),
        );

        $this->Model->bindModel('author');
        $this->Model->bindModel('categories', $categories);
        $records = $this->Model->getCollection($where, $params);

        if (count($records) > 0) {
            $records = $this->mergeAdditionalFields($records);
        }

        $html = $this->_renderList($records);

        // Writing to cache
        if ($this->cached) {
            $this->Cache->write($html, $this->cacheKey, $this->cacheTags);
        }

        return $this->_view($html);
    }

    /**
     * View entity. Entity ID must be integer and not null.
     */
    public function view($id = null) {
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);
        \ACL::turnUser(array($this->module, 'view_materials'), true);
        if (!empty($id) && !is_numeric($id)) {
            return $this->showMessage(__('Value must be numeric'));
        }
        if ($id < 1) {
            return $this->showMessage(__('Material not found'));
        }

        // Reading entity from cache
        if ($this->cached && $this->Cache->check($this->module . '_entity_' . $id)) {
            $entity = unserialize($this->Cache->read($this->module . '_entity_' . $id));
        } else {
            $this->Model->bindModel('author');
            $this->Model->bindModel('categories');
            $entity = $this->Model->getById($id);

            $entity = $this->mergeAdditionalFields(array($entity));
            $entity = $entity[0];

            // Set cache tags
            $this->setCacheTag(array(
                'user_id_' . $entity->getAuthor_id(),
                'record_id_' . $entity->getId(),
            ));

            // Writing to cache
            if ($this->cached) {
                $this->Cache->write(serialize($entity), $this->module . '_entity_' . $id, $this->cacheTags);
            }
        }

        if (!$entity) {
            return $this->showMessage(__('Material not found'));
        }

        $categories = $entity->getCategories();
        foreach ($categories as $category) {
            if (\ACL::checkAccessInList($category->getNo_access())) {
                return $this->showMessage(__('Permission denied'));
            }
        }

        // Category block is based on first category
        $this->buildCategoriesMarker($categories[0]->getId());

        /* COMMENT BLOCK */
        $this->comments_form = ['access' => 0];
        if (\Config::read('comment_active', $this->module) == 1 && \ACL::turnUser(array($this->module, 'view_comments')) && $entity->getCommented() == 1) {
            if (\ACL::turnUser(array($this->module, 'add_comments')))
                $this->comments_form = $this->_add_comment_form($id);
            $this->comments = $this->_get_comments($id);
        }
        $this->Register['current_vars'] = $entity;

        // Reading from cache
        if ($this->cached && $this->Cache->check($this->cacheKey)) {
            $html = $this->Cache->read($this->cacheKey);
            return $this->_view($html);
        }

        //производим замену соответствующих участков в html шаблоне нужной информацией
        $this->page_title = h($entity->getTitle());

        foreach ($categories as $index => $category) {
            $category->setUrl(get_url($this->getModuleURL('category/' . $category->getId())));
        }

        $navi = array();
        $navi['add_link'] = (\ACL::turnUser(array($this->module, 'add_materials'))) ? get_link(__('Add material'), $this->getModuleURL('add/')) : '';
        $navi['module_url'] = get_url($this->getModuleURL());

        $navi['navigation'] = $this->buildBreadCrumbs($entity->getCategory_id(), $entity->getTitle());

        $this->_globalize($navi);


        // New marker contains array with all categories
        $entity->setCategories($categories);

        // Old markers are based on first category
        $entity->setCategory($categories[0]);
        $entity->setCategory_name($categories[0]->getTitle());
        $entity->setCategory_url($categories[0]->getUrl());

        $entity->setProfile_url(getProfileUrl($entity->getAuthor_id()));
        $entry_url = get_url(entryUrl($entity, $this->module));
        $entity->setEntry_url($entry_url);

        $entity->setMain(WWW_ROOT . $this->getImagesPath($entity->getFilename()));
        $entity->setFoto_alt(h(preg_replace('#[^\w\d ]+#ui', ' ', $entity->getTitle())));
        $entity->setDescription(\PrintText::print_page($entity->getDescription(), $entity->getAuthor() ? $entity->getAuthor()->geteStatus() : 0));

        $next_prev = $this->Model->getNextPrev($id);
        $prev_id = (!empty($next_prev['prev'])) ? $next_prev['prev']->getId() : $id;
        $next_id = (!empty($next_prev['next'])) ? $next_prev['next']->getId() : $id;

        $entity->setPrevious_url(get_url($this->getModuleURL('view/' . $prev_id)));
        $entity->setNext_url(get_url($this->getModuleURL('view/' . $next_id)));


        $html = $this->render('material.html.twig', array('entity' => $entity));

        // Добавляем просмотр
        if (!$this->material_are_viewed($id)) {
            getDB()->save($this->module, array(
                'id' => $id,
                'views' => ($entity->getViews() + 1)
            ));
        }

        // Writing to cache
        if ($this->cached) {
            $this->Cache->write($html, $this->cacheKey, $this->cacheTags);
        }

        return $this->_view($html);
    }

    /**
     * Show materials by user. User ID must be integer and not null.
     */
    public function user($id = null) {
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);

        if (!empty($id) && !is_numeric($id)) {
            return $this->showMessage(__('Value must be numeric'));
        }
        if ($id < 1) {
            return $this->showMessage(__('Can not find user'));
        }

        $usersModel = \OrmManager::getModelInstance('Users');
        $user = $usersModel->getById($id);
        if (!$user) {
            return $this->showMessage(__('Can not find user'));
        }
        if (\ACL::checkAccessInList($user->getNo_access())) {
            return $this->showMessage(__('Permission denied'));
        }

        $this->page_title = sprintf(__('User materials'), ' "' . h($user->getName()) . '"');

        //формируем блок со списком  разделов
        $this->buildCategoriesMarker();

        // Reading from cache
        if ($this->cached && $this->Cache->check($this->cacheKey)) {
            $html = $this->Cache->read($this->cacheKey);
            return $this->_view($html);
        }

        // we need to know whether to show hidden
        $where = array('author_id' => $id);
        $where = filter($where);

        $total = $this->Model->getTotal(array('cond' => $where));
        $perPage = intval(\Config::read('per_page', $this->module));
        if ($perPage < 1) {
            $perPage = 10;
        }
        list ($pages, $page) = pagination($total, $perPage, $this->getModuleURL('user/' . $id));
        $this->Register['pages'] = $pages;
        $this->Register['page'] = $page;
        //$this->page_title .= ' (' . $page . ')';

        $navi = array();
        $navi['add_link'] = (\ACL::turnUser(array($this->module, 'add_materials'))) ? get_link(__('Add material'), $this->getModuleURL('add/')) : '';
        $navi['navigation'] = $this->buildBreadCrumbs(false, sprintf(__('User materials'), ' "' . h($user->getName()) . '"'));
        $navi['pagination'] = $pages;

        $cntPages = ceil($total / $perPage);
        $recOnPage = ($page == $cntPages) ? ($total % $perPage) : $perPage;
        $firstOnPage = ($page - 1) * $perPage + 1;
        $lastOnPage = $firstOnPage + $recOnPage - 1;

        $navi['meta'] = __('Count all material') . ' ' . $total . '. ' . ($total > 1 ? __('Count visible') . ' ' . $firstOnPage . '-' . $lastOnPage : '');
        $navi['category_name'] = sprintf(__('User materials'), ' "' . h($user->getName()) . '"');
        $this->_globalize($navi);

        if ($total <= 0) {
            $html = $this->render('list.html.twig');
            return $this->_view($html);
        }

        $order = getOrderParam($this->module);
        $params = array(
            'page' => $page,
            'limit' => $perPage,
            'order' => (empty($order) ? 'date DESC' : $order),
        );


        $this->Model->bindModel('author');
        $this->Model->bindModel('categories');
        $records = $this->Model->getCollection($where, $params);

        if (count($records) > 0) {
            $records = $this->mergeAdditionalFields($records);
        }

        if (\Config::read('use_local_preview', $this->module)) {
            $preview_size_x = \Config::read('img_size_x', $this->module);
            $preview_size_y = \Config::read('img_size_y', $this->module);
        } else {
            $preview_size_x = \Config::read('img_size_x');
            $preview_size_y = \Config::read('img_size_y');
        }

        // create markers
        foreach ($records as $entity) {
            $this->Register['current_vars'] = $entity;

            $entry_url = get_url(entryUrl($entity, $this->module));
            $entity->setEntry_url($entry_url);

            $entity->setPreview_foto(WWW_ROOT . $this->getImagesPath($entity->getFilename(), null, $preview_size_x, $preview_size_y));
            $entity->setFoto_alt(h(preg_replace('#[^\w\d ]+#ui', ' ', $entity->getTitle())));


            $entity_categories = $entity->getCategories();
            foreach ($entity_categories as $entity_category) {
                $entity_category->setUrl(get_url($this->getModuleURL('category/' . $entity_category->getId())));
            }
            // New marker contains array with all categories
            $entity->setCategories($entity_categories);

            // Old markers are based on first category
            $entity->setCategory($entity_categories[0]);
            $entity->setCategory_name($entity_categories[0]->getTitle());
            $entity->setCategory_url($entity_categories[0]->getUrl());

            $entity->setProfile_url(getProfileUrl($entity->getAuthor_id()));

            // Set cache tags
            $this->setCacheTag(array(
                'user_id_' . $entity->getAuthor_id(),
                'record_id_' . $entity->getId(),
            ));
        }

        $html = $this->render('list.html.twig', array('entities' => $records));

        // Writing to cache
        if ($this->cached) {
            $this->Cache->write($html, $this->cacheKey, $this->cacheTags);
        }

        return $this->_view($html);
    }

    /**
     * return form to add
     */
    public function add() {
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);
        \ACL::turnUser(array($this->module, 'add_materials'), true);

        // Если переданы данные формы
        if (isset($_POST['title']))
            return $this->_add();

        // categories block
        $this->buildCategoriesMarker();

        // Navigation panel
        $navi = array();
        $navi['add_link'] = (\ACL::turnUser(array($this->module, 'add_materials'))) ? get_link(__('Add material'), $this->getModuleURL('add/')) : '';
        $navi['navigation'] = $this->buildBreadCrumbs(false, __('Add material'));

        $this->page_title = __('Adding material');

        $this->_globalize($navi);

        // Additional fields
        $markers = array();
        $addFields = $this->getAdditionalFields();
        foreach ($addFields as $k => $field) {
            $markers[strtolower($k)] = $field;
        }

        $sectionsModel = \OrmManager::getModelInstance($this->module . 'Categories');
        $categories = $this->checkCategories($sectionsModel->getCollection());
        $markers['cats_selector'] = $this->buildCategoriesSelector($categories, false);
        $markers['cats_list'] = $categories;

        //comments and hide
        $markers['commented'] = ' checked="checked"';
        if (!\ACL::turnUser(array($this->module, 'record_comments_management'))) {
            $markers['commented'] = ' disabled="disabled"';
        }

        $markers['action'] = get_url($this->getModuleURL('add/'));

        $html = $this->render('addform.html.twig', array('context' => $markers));
        return $this->_view($html);
    }

    /**
     *
     * \Validate data and create a new record into
     * Data Base. If an errors, redirect user to add form
     * and show error message where speaks as not to admit
     * errors in the future
     *
     */
    private function _add() {
        // Если не переданы данные формы - функция вызвана по ошибке
        if (!isset($_POST['mainText']) || !isset($_POST['title']) || !isset($_POST['cats_selector'])) {
            return $this->showMessage(__('Some error occurred'), getReferer(), 'error', true);
        }
        $error = '';

        // Check additional fields if an exists.
        // This must be doing after define $error variable.
        $addFields = $this->checkAdditionalFields();
        if (is_string($addFields)) {
            $error .= $addFields;
        }

        // Обрезаем переменные до длины, указанной в параметре maxlength тега input
        $title = trim(mb_substr($_POST['title'], 0, 128));
        $description = trim($_POST['mainText']);
        $commented = (!empty($_POST['commented'])) ? 1 : 0;

        // Очищаем от опасных HTML тегов
        $description = \PrintText::getPurifedHtml($description);

        if (is_array($_POST['cats_selector']) and \Config::read('use_multicategories')) {
            // передан массив и разрешены мультикатегории
            $in_cat = implode(',', $_POST['cats_selector']);
        } elseif (is_array($_POST['cats_selector'])) {
            // передан массив, но запрещены мультикатегории
            $in_cat = $_POST['cats_selector'][0];
        } else {
            // передана одна категория
            $in_cat = intval($_POST['cats_selector']);
        }

        if (!\ACL::turnUser(array($this->module, 'record_comments_management'))) {
            $commented = 1;
        }

        // Проверяем, заполнены ли обязательные поля
        //validation data class
        if (empty($in_cat)) {
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Category')) . '</li>' . "\n";
        }
        if (empty($title)) {
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Foto name')) . '</li>' . "\n";
        } elseif (!\Validate::cha_val($title, V_TITLE)) {
            $error .= '<li>' . sprintf(__('Wrong chars in field "param"'), __('Foto name')) . '</li>' . "\n";
        }
        $max_length = \Config::read('description_length', $this->module);
        if ($max_length <= 0) {
            $max_length = 1000;
        }
        if (mb_strlen($description) > $max_length) {
            $error .= '<li>' . sprintf(__('Very big "param"'), __('Description'), $max_length) . '</li>' . "\n";
        }


        /* check file */
        if (empty($_FILES['foto']['name'])) {
            $error .= '<li>' . __('No attachment foto') . '</li>' . "\n";
        } else {
            if ($_FILES['foto']['size'] > $this->getMaxSize())
                $error .= '<li>' . sprintf(__('Very big file'), $_FILES['foto']['name'], getSimpleFileSize($this->getMaxSize())) . '</li>' . "\n";
            if (!isImageFile($_FILES['foto']))
                $error .= '<li>' . sprintf(__('Wrong file format'), $_FILES['foto']['name']) . '</li>' . "\n";
        }

        $sectionsModel = \OrmManager::getModelInstance($this->module . 'Categories');

        $in_cat_array = explode(',', $in_cat);
        foreach ($in_cat_array as $cat) {
            $category = $sectionsModel->getById($cat);
            if (!$category || \ACL::checkAccessInList($category->getNo_access())) {
                $error .= '<li>' . __('Can not find category') . $cat . '</li>' . "\n";
                break;
            }
        }

        // Errors
        if (!empty($error)) {
            $error_msg = '<p class="errorMsg">' . __('Some error in form') . '</p>'
                    . "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
            return $this->showMessage($error_msg, $this->getModuleURL("add/"));
        }


        // Защита от того, чтобы один пользователь не добавил
        // 100 материалов за одну минуту
        if (isset($_SESSION['unix_last_post']) and ( time() - $_SESSION['unix_last_post'] < 10 )) {
            return $this->showMessage(__('Material has been added'), $this->getModuleURL("add/"));
        }

        // Формируем SQL-запрос на добавление темы
        $data = array(
            'title' => $title,
            'description' => mb_substr($description, 0, $max_length),
            'date' => (new \DateTime())->format('Y-m-d H:i:s'),
            'author_id' => \UserAuth::getField('id'),
            'category_id' => $in_cat,
            'filename' => '',
            'commented' => $commented,
        );

        // Save additional fields
        $data = array_merge($data, $addFields);

        $className = \OrmManager::getEntityName($this->module);
        $entity = new $className($data);
        if ($entity) {
            $entity->setId($entity->save());


            /* save full and resample images */
            $ext = strtolower(strchr($_FILES['foto']['name'], '.'));
            $files_dir = ROOT . '/data/images/' . $this->module . '/';

            if (!file_exists($files_dir)) {
                mkdir($files_dir, 0766);
            }
            $save_path = $files_dir . $entity->getId() . $ext;

            if (!move_uploaded_file($_FILES['foto']['tmp_name'], $save_path)) {
                $error_flag = true;
            } elseif (!chmod($save_path, 0644)) {
                $error_flag = true;
            }

            /* if an error when coping */
            if (!empty($error_flag) && $error_flag) {
                $entity->delete();
                $error_msg = '<p class="errorMsg">' . __('Some error in form') . '</p>'
                        . "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
                return $this->showMessage($error_msg, $this->getModuleURL('add/'));
            }

            $entity->setFilename($entity->getId() . $ext);
            $entity->save();


            // Create watermark and resample image
            $watermark_path = ROOT . '/data/img/' . (\Config::read('watermark_type') == '1' ? 'watermark_text.png' : \Config::read('watermark_img'));
            if (\Config::read('use_watermarks') && !empty($watermark_path) && file_exists($watermark_path)) {
                $waterObj = new \AtmImg;
                $waterObj->createWaterMark($save_path, $watermark_path);
            }

            // hook for plugins
            \Events::init('new_entity', array(
                'entity' => $entity,
                'module' => $this->module,
            ));

            // Clean cache
            $this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module));
            if ($this->isLogging) {
                \Logination::write('adding ' . $this->module, $this->module . ' id(' . $entity->getId() . ')');
            }

            return $this->showMessage(__('Material has been added'), entryUrl($entity, $this->module), 'ok');
        } else {
            return $this->showMessage(__('Some error occurred'), $this->getModuleURL('add/'));
        }
    }

    /**
     *
     * Create form and fill his data from record which ID
     * transfered into function. Show errors if an exists
     * after unsuccessful attempt. Also can get data for filling
     * from SESSION if user try preview message or create error.
     *
     * @param int $id material then to be edit
     */
    public function edit($id = null) {
        if (!empty($id) && !is_numeric($id)) {
            return $this->showMessage(__('Value must be numeric'));
        }
        if ($id < 1) {
            return $this->showMessage(__('Material not found'));
        }

        // Если переданы данные формы
        if (isset($_POST['title']))
            return $this->_update($id);

        $this->page_title = __('Material editing');

        $this->Model->bindModel('author');
        $this->Model->bindModel('categories');
        $entity = $this->Model->getById($id);

        if (!$entity) {
            return $this->showMessage(__('Material not found'), $this->getModuleURL());
        }

        \ACL::turnUser(array($this->module, 'view_list'), true);
        if (!\ACL::turnUser(array($this->module, 'edit_materials')) && (\UserAuth::isUser() && $entity->getAuthor_id() == \UserAuth::getField('id') && \ACL::turnUser(array($this->module, 'edit_mine_materials'))) === false) {
            return $this->showMessage(__('Permission denied'), entryUrl($entity, $this->module));
        }

        foreach ($entity->getCategories() as $cat) {
            if (!$cat || \ACL::checkAccessInList($cat->getNo_access())) {
                return $this->showMessage(__('Permission denied'), getReferer(), 'error', true);
            }
        }

        if (count($entity) > 0) {
            $entity = $this->mergeAdditionalFields(array($entity));
            $entity = $entity[0];
        }

        $this->Register['current_vars'] = $entity;

        $entity_categories = $entity->getCategories();

        // Category block is based on first category
        $this->buildCategoriesMarker($entity_categories[0]->getId());

        $entity->setMain_text(\PrintText::print_page($entity->getDescription(), $entity->getAuthor() ? $entity->getAuthor()->getStatus() : 0));

        $sectionsModel = \OrmManager::getModelInstance($this->module . 'Categories');
        $categories = $this->checkCategories($sectionsModel->getCollection());
        $entity->setCats_selector($this->buildCategoriesSelector($categories, $entity->getCategory_id()));
        $entity->setCats_list($categories);
        $entity->setIn_cat($entity->getCategory_id());

        //comments and hide
        $commented = ($entity->getCommented()) ? 'checked="checked"' : '';
        if (!\ACL::turnUser(array($this->module, 'record_comments_management'))) {
            $commented .= ' disabled="disabled"';
        }
        $entity->setAction(get_url($this->getModuleURL('edit/' . $entity->getId())));
        $entity->setCommented($commented);

        foreach ($entity_categories as $index => $category) {
            $category->setUrl(get_url($this->getModuleURL('category/' . $category->getId())));
        }

        // New marker contains array with all categories
        $entity->setCategories($entity_categories);

        // Navigation panel
        $navi = array();
        $navi['add_link'] = (\ACL::turnUser(array($this->module, 'add_materials'))) ? get_link(__('Add material'), $this->getModuleURL('add/')) : '';

        // Old markers are based on first category
        $entity->setCategory($entity_categories[0]);
        $entity->setCategory_name($entity_categories[0]->getTitle());
        $entity->setCategory_url($entity_categories[0]->getUrl());
        $navi['category_name'] = $entity_categories[0]->getTitle();
        $navi['category_url'] = $entity_categories[0]->getUrl();

        $navi['navigation'] = $this->buildBreadCrumbs($entity->getCategory_id(), array(
            get_link($entity->getTitle(), get_url(entryUrl($entity, $this->module))),
            __('Material editing')
        ));
        $this->_globalize($navi);

        $html = $this->render('editform.html.twig', array('context' => $entity));
        return $this->_view($html);
    }

    /**
     *
     * \Validate data and update record into
     * Data Base. If an errors, redirect user to add form
     * and show error message where speaks as not to admit
     * errors in the future
     *
     */
    private function _update($id) {
        // Если не переданы данные формы - функция вызвана по ошибке
        if (!isset($id) || !isset($_POST['title'])) {
            return $this->showMessage(__('Some error occurred'));
        }

        $error = '';

        $entity = $this->Model->getById($id);
        if (!$entity) {
            return $this->showMessage(__('Material not found'));
        }

        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);
        if (!\ACL::turnUser(array($this->module, 'edit_materials')) && (\UserAuth::isUser() && $entity->getAuthor_id() == \UserAuth::getField('id') && \ACL::turnUser(array($this->module, 'edit_mine_materials'))) === false) {
            return $this->showMessage(__('Permission denied'), getReferer(), 'error', true);
        }

        // Check additional fields if an exists.
        // This must be doing after define $error variable.
        $addFields = $this->checkAdditionalFields();
        if (is_string($addFields)) {
            $error .= $addFields;
        }

        // Обрезаем переменные до длины, указанной в параметре maxlength тега input
        $title = trim(mb_substr($_POST['title'], 0, 128));
        $description = trim($_POST['mainText']);
        $commented = (!empty($_POST['commented'])) ? 1 : 0;

        // Очищаем опасных HTML тегов
        $description = \PrintText::getPurifedHtml($description);

        if (empty($_POST['cats_selector'])) {
            $in_cat = $entity->getCategory_id();
        } elseif (is_array($_POST['cats_selector']) and \Config::read('use_multicategories')) {
            // передан массив и разрешены мультикатегории
            $in_cat = implode(',', $_POST['cats_selector']);
        } elseif (is_array($_POST['cats_selector'])) {
            // передан массив, но запрещены мультикатегории
            $in_cat = $_POST['cats_selector'][0];
        } else {
            // передана одна категория
            $in_cat = intval($_POST['cats_selector']);
        }

        if (!\ACL::turnUser(array($this->module, 'record_comments_management'))) {
            $commented = 1;
        }

        // Проверяем, заполнены ли обязательные поля
        if (empty($title)) {
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Foto name')) . '</li>' . "\n";
        } elseif (!\Validate::cha_val($title, V_TITLE)) {
            $error .= '<li>' . sprintf(__('Wrong chars in field "param"'), __('Foto name')) . '</li>' . "\n";
        }
        $max_length = \Config::read('description_length', $this->module);
        if ($max_length <= 0) {
            $max_length = 1000;
        }
        if (mb_strlen($description) > $max_length) {
            $error .= '<li>' . sprintf(__('Very big "param"'), __('Description'), $max_length) . '</li>' . "\n";
        }

        $sectionsModel = \OrmManager::getModelInstance($this->module . 'Categories');

        $in_cat_array = explode(',', $in_cat);
        foreach ($in_cat_array as $cat) {
            $category = $sectionsModel->getById($cat);
            if (!$category || \ACL::checkAccessInList($category->getNo_access())) {
                $error .= '<li>' . __('Can not find category') . $cat . '</li>' . "\n";
                break;
            }
        }

        // Errors
        if (!empty($error)) {
            $error_msg = '<p class="errorMsg">' . __('Some error in form') . '</p>'
                    . "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
            return $this->showMessage($error_msg, $this->getModuleURL("edit/$id"));
        }

        // Clean cache
        $this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module, 'record_id_' . $id));

        $data = array(
            'title' => $title,
            'category_id' => $in_cat,
            'description' => mb_substr($description, 0, $max_length),
            'commented' => $commented,
        );

        // Save additional fields
        $data = array_merge($data, $addFields);

        $entity->set($data);
        $entity->save();

        if ($this->isLogging) {
            \Logination::write('editing ' . $this->module, $this->module . ' id(' . $id . ')');
        }

        return $this->showMessage(__('Material is saved'), entryUrl($entity, $this->module), 'ok');
    }

    /**
     * Check user access and if all right
     * delete record with geting ID.
     *
     * @param int $id
     */
    public function delete($id = null) {
        $this->cached = false;
        if (!empty($id) && !is_numeric($id)) {
            return $this->showMessage(__('Value must be numeric'));
        }
        if ($id < 1) {
            return $this->showMessage(__('Material not found'));
        }

        $entity = $this->Model->getById($id);
        if (!$entity) {
            return $this->showMessage(__('Material not found'));
        }

        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);
        if (!\ACL::turnUser(array($this->module, 'delete_materials')) && (\UserAuth::isUser() && $entity->getAuthor_id() == \UserAuth::getField('id') && \ACL::turnUser(array($this->module, 'delete_mine_materials'))) === false) {
            return $this->showMessage(__('Permission denied'), getReferer(), 'error', true);
        }

        // Clean cache
        $this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module, 'record_id_' . $id));

        $entity->delete();

        $user_id = (\UserAuth::isUser()) ? intval(\UserAuth::getField('id')) : 0;
        if ($this->isLogging) {
            \Logination::write('delete ' . $this->module, $this->module . ' id(' . $id . ') user id(' . $user_id . ')');
        }

        $referer_params = explode("/", trim(parse_url(getReferer(), PHP_URL_PATH), '/'));

        return $this->showMessage(
                        __('Material has been delete'), ((!isset($referer_params[1]) or in_array($referer_params[1], array("category", "index"))) ? getReferer() : false), 'ok'
        );
    }

    /**
     * @param int $id - record ID
     *
     * update date by record also up record in recods list
     */
    public function upper($id) {
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'), true);
        \ACL::turnUser(array($this->module, 'up_materials'), true);
        if (!empty($id) && !is_numeric($id)) {
            return $this->showMessage(__('Value must be numeric'));
        }
        if ($id < 1) {
            return $this->showMessage(__('Material not found'));
        }

        $entity = $this->Model->getById($id);
        if ($entity) {
            $entity->setDate(date("Y-m-d H:i:s"));
            $entity->save();
            return $this->showMessage(__('Operation is successful'), false, 'alert');
        }
        return $this->showMessage(__('Some error occurred'));
    }

    /**
     * RSS
     *
     */
    public function rss() {
        \ACL::turnUser(array($this->module, 'view_list'), true);

        $options = array(
            'query_params' => array(
                'cond' => array('available' => '1')
            ),
            "fields_item" => array(
                "enclosure" => function($record, $sitename) {
                    $images = array();
                    $images[] = array(
                        "url" => $sitename . $this->getImagesPath($record->getFilename()),
                        "type" => 'image/' . substr(strrchr($record->getFilename(), "."), 1)
                    );
                    return $images;
                }
                    )
                );

                $this->_rss($options);
            }

        }
