<?php
/**
* @project    Atom-M CMS
* @package    LoadsSections Entity
* @url        https://atom-m.modos189.ru
*/


namespace LoadsModule\ORM;

class LoadsCategoriesEntity extends \OrmEntity
{

    protected $id;
    protected $parent_id;
    protected $announce;
    protected $title;
    protected $view_on_home;
    protected $no_access;


    public function getListKeys() {
        return array_keys(get_object_vars($this));
    }


    public function __getAPI() {
        return array(
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'announce' => $this->announce,
            'title' => $this->title,
            'view_on_home' => $this->view_on_home,
            'no_access' => $this->no_access,
        );
    }

}
