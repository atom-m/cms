<?php
/**
* @project    Atom-M CMS
* @package    Search Module
* @url        https://atom-m.modos189.ru
*/



namespace SearchModule;

class ActionsHandler extends \Module {

    /**
     * @module_title  title of module
     */
    public $module_title = 'Поиск';

    /**
     * @module module indentifier
     */
    public $module = 'search';

    /**
     * @var int
     */
    private $minInputStr = 5;

    function __construct($params) {
        parent::__construct($params);

        $this->setModel();
    }

    /**
     * @return string - $this->_view
     *
     * Doing search and build page with results
     */
    public function index() {
        // Check index
        $this->__checkIndex();

        $minInput = \Config::read('min_length', $this->module);
        if (!empty($minInput)) {
            $this->minInputStr = (int) $minInput;
        }

        $html = null;
        $error = null;
        $results = null;

        // List of modules for search
        $modules = array();
        $search_modules = array();
        $installed_modules = \ModuleManager::getInstalledModules();
        if (!empty($installed_modules) && is_array($installed_modules)) {
            foreach($installed_modules as $module) {
                $model_name = \OrmManager::getModelName($module);
                if (\Config::read('active', $module) && method_exists($model_name, '__getQueriesForSearch')) {
                    $modules[] = $module;
                    if (isset($_GET['m']) && is_array($_GET['m']) && in_array($module, $_GET['m'])) {
                        $search_modules[] = $module;
                    }
                }
            }
        }
        $search_modules = count($search_modules) ? $search_modules : $modules;
        
        $_SESSION['m'] = $search_modules;

        if (isset($_GET['q']) || isset($_GET['q'])) {
            $str = (isset($_GET['q'])) ? h($_GET['q']) : '';
            if (empty($str))
                $str = (isset($_GET['q'])) ? h($_GET['q']) : '';
            if (!is_string($str))
                $str = (string) $str;
            $str = trim($str);


            if (empty($str) || mb_strlen($str) < $this->minInputStr)
                $error = $error . sprintf(__('Very small query'), $this->minInputStr);


            if ($this->cached) {
                $this->cacheKey .= '_' . md5($str);
                if ($this->Cache->check($this->cacheKey)) {
                    $html = $this->Cache->read($this->cacheKey);
                    return $this->_view($html);
                }
            }

            $_SESSION['search_query'] = $str;
            if (!empty($error)) {
                $_SESSION['errorForm'] = array();
                $_SESSION['errorForm']['errors'] = $error;
            } else {
                $results = $this->__search($str, $search_modules);
                if (!is_array($results) || count($results) <= 0) {
                    $error = __('No results'); // TODO
                }
            }
        } else {
            $_SESSION['search_query'] = '';
        }


        // Nav block
        $nav = array();
        $nav['navigation'] = $this->buildBreadCrumbs();
        $this->_globalize($nav);


        $this->page_title = $this->module_title;
        if (!empty($_GET['q']))
            $this->page_title .= ' - ' . h($_GET['q']);


        $source = $this->render('search_list.html.twig', array(
            'context' => array(
                'results' => $results,
                'error' => $error,
            ),
            'form' => $this->__form($modules)
        ));


        //write into cache
        if ($this->cached && !empty($str)) {
            //set users_id that are on this page
            $this->setCacheTag(array(
                'search_str_' . $str,
            ));
            $this->cacheKey .= '_' . md5($str);
            $this->Cache->write($source, $this->cacheKey, $this->cacheTags);
        }

        return $this->_view($source);
    }

    /**
     * @return string search form
     */
    private function __form($modules) {
        $markers = array(
            'action' => get_url($this->getModuleURL()),
            'search' => '',
        );

        if (!empty($modules) && is_array($modules)) {
            foreach($modules as $module) {
                $markers[$module] = isset($_SESSION['m']) && is_array($_SESSION['m']) && in_array($module, $_SESSION['m']) ? 'checked' : '0';
            }
        } else {
            $modules = false;
        }

        // If an errors
        if (isset($_SESSION['errorForm'])) {
            $markers['info'] = $this->render('infomessage.html.twig', array('context' => array('message' => $_SESSION['errorForm']['errors'])));
            unset($_SESSION['errorForm']);
        }

        $markers['search'] = $_SESSION['search_query'];

        return array(
            'context' => $markers,
            'modules' => $modules,
        );
    }

    /**
     * @return boolean
     */
    private function __checkIndex() {
        $meta_file = ROOT . $this->getTmpPath('meta.dat');
        if (file_exists($meta_file) && is_readable($meta_file)) {
            $meta = unserialize(file_get_contents($meta_file));
            if (!empty($meta['expire']) && $meta['expire'] > time()) {
                return true;
            } else {
                $this->__createIndex();
            }
        } else {
            touchDir(ROOT . $this->getTmpPath());
            $this->__createIndex();
        }

        $index_interval = intval(\Config::read('index_interval', $this->module));
        if ($index_interval < 1)
            $index_interval = 1;
        $meta['expire'] = (time() + ($index_interval * 84000));
        file_put_contents($meta_file, serialize($meta));
        return true;
    }

    /**
     * @param string $str
     * @return array
     *
     * Send request and return search results
     */
    private function __search($str, $modules) {
        $words = explode(' ', $str);
        $_words = array();
        foreach ($words as $key => $word) {
            $word = $this->__filterText($word);
            if (mb_strlen($word) < $this->minInputStr) {
                continue;
            }
            $_words[] = $word;
        }
        if (count($_words) < 1)
            return array();
        $string = resc(implode('* ', $_words) . '*');

        //query
        $limit = intval(\Config::read('per_page', $this->module));
        if ($limit < 1) {
            $limit = 10;
        }
        $results = $this->Model->getSearchResults($string, $limit, $modules);
        foreach ($results as &$result) {
            $announce = str_ireplace(mb_strtolower($str), '<strong>' . mb_strtolower($str) . '</strong>', \PrintText::print_page($result->getIndex()));
            
            // Replace image tags in text
            $attaches = $result->getAttaches();
            if ($attaches && count($attaches)) {
                foreach ($attaches as $attach) {
                    if ($attach->getIs_image() == '1') {
                        $announce = $this->insertImageAttach($announce, $attach->getFilename(), $attach->getAttach_number(), $result->getModule());
                    }
                }
            }
            $result->setAttaches(null);
            $result->setAnnounce($announce);
        }
        return $results;
    }

    /**
     *
     *
     * Create index for search engine
     */
    private function __createIndex() {
        if (function_exists('ignore_user_abort'))
            ignore_user_abort();
        if (function_exists('set_time_limit'))
            set_time_limit(180);

        $this->Model->createIndex();
    }

    /**
     * @param string $str
     * @return string
     *
     * Cut HTML and BB tags. Also another chars
     */
    private function __filterText($str) {
        $str = preg_replace('#<[^>]*>|\[[^\]]*\]|[,\.=\'"\|\{\}/\\_\+\?\#<>:;\)\(`\-0-9]#iu', '', $str);
        //$str = preg_replace('#(^| )[^ ]{1,2}( |$)#iu', ' ', $str);
        //$str_to_array = explode(' ', mb_strtolower($str));
        //$str_to_array = array_unique($str_to_array);
        //$str = implode(' ', $str_to_array);
        return (!empty($str)) ? $str : false;
    }

}
