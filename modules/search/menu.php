<?php
return array(
    'icon_class' => 'mdi-action-find-in-page',
    'pages'   => array(
        WWW_ROOT.'/admin/settings.php?m=search'             => __('Settings'),
    ),
);