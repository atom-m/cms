<?php
/**
* @project    Atom-M CMS
* @package    Pages Module
* @url        https://atom-m.modos189.ru
*/

namespace PagesModule;

Class ActionsHandler extends \Module {

    /**
     * @module_title  title of module
     */
    public $module_title = 'Страницы';

    /**
     * @template  layout for module
     */
    public $template = 'pages';

    /**
     * @module module indentifier
     */
    public $module = 'pages';


    function __construct($params) {
        parent::__construct($params);

        $this->setModel();


    }

    /**
     * default action
     */
    function index($id = null) {
        if (empty($id)) {
            redirect('/');
        }

        // Load page by ID
        if (!preg_match('#^[\da-zа-я_\-./]+$#iu', $id)) {
            http_response_code(404);
            include_once R.'sys/inc/error.php';
            die();
        }

        if ($this->cached && $this->Cache->check($this->cacheKey)) {
            // Reading from cache
            $page = unserialize($this->Cache->read($this->cacheKey));
        } else {
            $page = (is_numeric($id) && $id >= 2) ? $this->Model->getById($id) : $this->Model->getByUrl($id);
            if (!$page) {
                http_response_code(404);
                include_once R.'sys/inc/error.php';
                die();
            }

            // Writing to cache
            if ($this->cached) {
                $this->Cache->write(serialize($page), $this->cacheKey, $this->cacheTags);
            }
        }
        $id = $page->getId();

        $this->page_title = $page->getName();
        $this->page_meta_keywords = $page->getMeta_keywords();
        $this->page_meta_description = $page->getMeta_description();
        $source = $page->getContent();


        // Tree line
        if ($this->cached && $this->Cache->check('static_page_path_' . $page->getPath())) {
            // Reading from cache
            $append = unserialize($this->Cache->read('static_page_path_' . $page->getPath()));
        } else {
            $append = array();
            $cnots = explode('.', $page->getPath());
            foreach ($cnots as $index => $cnot) {
                if ($cnot == 1 || empty($cnot)) {
                    unset($cnots[$index]);
                }
            }
            if (count($cnots) > 0) {
                $ids = "'" . implode("', '", $cnots) . "'";
                $pages = $this->Model->getCollection(array(
                    "`id` IN (" . $ids . ")"
                ), array(
                    'order' => 'path',
                ));

                if (!empty($pages) && is_array($pages)) {
                    foreach ($pages as $p) {
                        $append[] = get_link(__($p->getName()), '/' . $this->Model->buildUrl($p->getId()));
                    }
                }
            }
            // Writing to cache
            if ($this->cached) {
                $this->Cache->write(serialize($append), 'static_page_path_' . $page->getPath(), $this->cacheTags);
            }
        }

        $append[] = h($page->getName());
        $navi['navigation'] = $this->buildBreadCrumbs(false, $append);

        $navi['page_id'] = $id;
        $this->_globalize($navi);

        if (!$page->getTemplate())
            $page_templ = 'default.html.twig';
        else
            $page_templ = $page->getTemplate();

        // disable using main.html for base in $this->_view()
        $this->wrap = false;
        return $this->_view($this->render($page_templ, array('content' => $source)));
    }
}
