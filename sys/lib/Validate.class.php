<?php
/**
* @project    Atom-M CMS
* @package    Validate class
* @url        https://atom-m.modos189.ru
*/


/*
 * REGEX for titles.
 * Allowed chars. You can change this.
 */
//define ('V_TITLE', '#^[A-ZА-Яа-яa-z0-9\s-\(\),\._\?\!\w\d\{\} ]+$#ui');
define ('V_TITLE', '#^[A-ZА-Яа-яa-z0-9ё\s\-(),._\?!\w\d\{\}\<\>:=\+&%\$\[\]\\\/"\'\#№]+$#ui');
define ('V_TITLE_NOHTML', '#^[A-ZА-Яа-яa-z0-9ё\s\-(),._\?!\w\d\{\}:=\+&%\$\[\]\\\/"\']+$#ui');


define ('V_INT', '#^\d+$#i');
define ('V_TEXT', '#^[\wA-ZА-Яа-яa-z0-9\s\-\(\):;\[\]\+!\.,&\?/\{\}="\']*$#uim');
define ('V_MAIL', '#^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,6}$#i');
define ('V_URL', '#^((https?|ftp):\/\/)?(www.)?([\w\d]+(-?[\w\d]+)*\.)+[\w]{2,6}(\/[\+\-\w\d=_.%\?&\/]*)*$#ui');
define ('V_CAPTCHA', '#^[\dabcdefghijklmnopqrstuvwxyz]+$#i');
define ('V_LOGIN', '#^[- _0-9A-Za-zА-Яа-я@]+$#ui');
define ('V_LOGIN_LATIN', '#^[- _0-9A-Za-z@]+$#ui');
define ('V_FULLNAME', '#^[A-ZА-Яа-яa-zё\s\-(),.\D\']+$#ui');
define ('V_CITY', '#^[- _a-zА-Яа-я@]+$#ui');

class Validate {

    /**
    * check for a pattern
    *
    * @param string $data
    * @param string $pattern
    *
    * @return bool
    */
    static function cha_val($data, $pattern = '#^\w*$#Uim')
    {
        return preg_match($pattern, $data);
    }


    /**
    * check length of value
    *
    * @param string $data
    * @param int $min
    * @param int $max
    *
    * @return string|true if check is fail then return error string else true
    */
    static function len_val($data, $min = 1, $max = 100)
    {
        if (mb_strlen($data) > $max)
            return __('Very long value');
        elseif (mb_strlen($data) < $min)
            return __('Very short value');
        else
            return true;
    }


    public static function check($module, $action, $wrap = false)
    {
        $form = self::prepareForm($module, $action);

        $Register = Register::getInstance();
        $errors = array();


        foreach ($form as $title_ => $params) {
            $fields = array();

            // multiple fialeds (for example, for attachments)
            if (!empty($params['for'])) {
                for ($i = intval($params['for']['from']); $i <= $params['for']['to']; $i++) {
                    $fields[] = $title_ . $i;
                }

            } else {
                $fields[] = $title_;
            }


            // process
            foreach ($fields as $field) {
                $title = (substr($field,0, 7) == 'files__') ? substr($field, 7) : $field;
                $publicTitle = (!empty($params['title'])) ? $params['title'] : $title_;


                // file or field
                if (substr($field, 0, 7) != 'files__') {
                    // required
                    if (!empty($params['required']) && $params['required'] === true) {
                        if (empty($_POST[$title])) 
                            $errors[] = self::getErrorMessage('required', $params, $title, $module);


                    } else if (!empty($params['required']) && $params['required'] === 'editable') {
                        $fields_settings = Config::read('fields', $module);
                        if (!$fields_settings) $fields_settings = array();

                        if (empty($_POST[$title]) && in_array($title, $fields_settings)) { 
                            $errors[] = self::getErrorMessage('required', $params, $title, $module);
                            continue;
                        }
                    }


                    // max length
                    if (!empty($params['max_lenght'])) {
                        if (!empty($_POST[$title]) && mb_strlen($_POST[$title]) > $params['max_lenght']) 
                            $errors[] = self::getErrorMessage('max_lenght', $params, $title, $module);
                    }

                    // min length
                    if (!empty($params['min_lenght'])) {
                        if (!empty($_POST[$title]) && mb_strlen($_POST[$title]) < $params['min_lenght']) 
                            $errors[] = self::getErrorMessage('min_lenght', $params, $title, $module);
                    }

                    // compare
                    if (!empty($params['compare'])) {
                        if (!empty($_POST[$title]) && $_POST[$title] != @$_POST[$params['compare']]) 
                            $errors[] = self::getErrorMessage('compare', $params, $title, $module);
                    }

                    // pattern
                    if (!empty($params['pattern'])) {
                        if (!empty($_POST[$title]) && !filter_var($_POST[$title], $params['pattern'])) 
                            $errors[] = self::getErrorMessage('pattern', $params, $title, $module);
                    }

                    // user function
                    if (!empty($params['function'])) {
                        if (!empty($_POST[$title]) && is_callable($params['function'])) 
                            $errors[] = $params['function']($errors);
                    }


                // field is file
                } else {

                    // required
                    if (!empty($params['required']) && $params['required'] === true) {
                        if (empty($_FILES[$title]) || empty($_FILES[$title]['name'])) 
                            $errors[] = self::getErrorMessage('required', $params, $title, $module);


                    } else if (!empty($params['required']) && $params['required'] === 'editable') {
                        $fields_settings = $Register['Config']->read('fields', $module);

                        if ((empty($_FILES[$title]) || empty($_FILES[$title]['name'])) && in_array($title, $fields_settings)) 
                            $errors[] = self::getErrorMessage('required', $params, $title, $module);
                    }


                    if (@empty($_FILES[$title]['name'])) continue;


                    // type
                    if (!empty($params['type'])) {
                        $ext = strrchr($_FILES[$title]['name'], ".");

                        switch ($params['type']) {
                            // image files
                            case 'image':
                                $img_extentions = array('.png','.jpg','.gif','.jpeg', '.PNG','.JPG','.GIF','.JPEG');

                                if (($_FILES[$title]['type'] != 'image/jpeg'
                                && $_FILES[$title]['type'] != 'image/jpg'
                                && $_FILES[$title]['type'] != 'image/gif'
                                && $_FILES[$title]['type'] != 'image/png')
                                || !in_array(strtolower($ext), $img_extentions)) {
                                    $errors[] = self::getErrorMessage('type', $params, $title, $module);
                                }
                                break;

                            // other files (for example loads module attaches)    
                            case 'file':
                                $denied_exts = array('.php', '.phtml', '.php3', '.html', '.htm', '.pl', 'js');
                                if (in_array(strtolower($ext), $denied_exts)) 
                                    $errors[] = self::getErrorMessage('type', $params, $title, $module);
                                break;
                        }
                    }

                    // size
                    if (!empty($params['max_size'])) {
                        //pr($title);
                        if ($_FILES[$title]['size'] > $params['max_size']) {
                            $errors[] = self::getErrorMessage('max_size', $params, $title, $module);
                        }
                    }
                }
            }
        }

        //if ($wrap === true && is_callable($this->layoutWrapper)) {
        //  $errors = $this->wrapErrors($errors);
        //}

        return $errors;
    }


    /**
     * @param string $module
     * @param string $action
     * @param bool $correct
     *
     * @return array
     */
    static function getFormPost($module, $action, $correct = false)
    {
        $fields = self::prepareForm($module, $action);

        foreach ($fields as $k => $value) {
            $fields[$k]['name'] = $k;
            $fields[$k]['value'] = (array_key_exists($k, $_POST)) ? $_POST[$k] : false;

            if ($correct === true)
                $fields[$k]['value'] = trim($fields[$k]['value']);
        }

        return $fields;
    }


    private static function getErrorMessage($type, $params, $title, $module) {
        $publicTitle = (!empty($params['title'])) ? $params['title'] : $title;

        // Try to translate title
        $publicTitle_ = __($publicTitle);
        if ($publicTitle_ === $publicTitle && $module) {
            $publicTitle = __($publicTitle, $module);
        }

        if (array_key_exists($type . '_error', $params)) return $params[$type . '_error'];

        switch ($type) {

            case 'required':
                $message = sprintf(__('Empty field "param"'), $publicTitle);
                break;

            case 'max_lenght':
                $message = sprintf(__('Very big "param"'), $publicTitle, $params['max_lenght']);
                break;

            case 'min_lenght':
                $message = sprintf(__('Very small "param"'), $publicTitle, $params['min_lenght']);
                break;

            case 'compare':
                $message = sprintf(__('Fields are differend'), $publicTitle);
                break;

            case 'pattern':
                $message = sprintf(__('Wrong chars in field "param"'), $publicTitle);
                break;

            case 'type':
                $message = __('Wrong file format');
                break;

            case 'max_size':
                $message = sprintf(__('Very big file'), $publicTitle, round(($params['max_size'] / 1000), 1));
                break;
        }

        return $message;
    }


    private static function prepareForm($module, $action)
    {
        $form = Config::read('forms', $module);
        $form = (array_key_exists($action, $form))
            ? $form[$action]
            : @$form[substr($action, 0, -5)];

        if (empty($form) || count($form) < 1)
            throw new Exception("Form ".$action." in ".$module." not found.");

        return $form;
    }

}

