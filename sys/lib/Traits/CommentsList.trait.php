<?php
/**
* @project    Atom-M CMS
* @package    Commented Module Class
* @url        https://atom-m.modos189.ru
*/

namespace Traits;

trait CommentsList {
    use AdditionalFields;

    /**
     * get comments
     *
     * @id (int)    entity ID
     * @return      html comments list
     */
    protected function _get_comments($id = null, $is_user = false) {

        function mkTree($comments, $parent = 0, $new = array(), $i = 0) {
            if ($comments) {
                foreach ($comments as $comment) {
                    if ($comment->getParent_id() == $parent) {
                        $comment->setLvl($i);
                        $new[] = $comment;
                        $new = mkTree($comments, $comment->getId(), $new, $i+1);
                    }
                }
                return $new;
            }
        }

        if (empty($id) || $id < 1) $data = true;

        $commentsModel = \OrmManager::getModelInstance('Comments');

        if (empty($data) && $commentsModel) {

            /* pages nav */
            $where = $is_user ? array('user_id' => $id) : array('entity_id' => $id, 'module' => $this->module);
            if (!\ACL::turnUser(array('__other__', 'can_see_hidden')))
                $where['premoder'] = 'confirmed';


            $order_way = (\Config::read('comments_order', $this->module)) ? 'DESC' : 'ASC';
            $params = array(
                'order' => 'date ' . $order_way,
            );
            $commentsModel->bindModel('author');
            $comments = $commentsModel->getCollection($where, $params);
            $comments = mkTree($comments);

            if (count($comments) > 0) {
                $comments = $this->mergeAdditionalFields($comments, 'comments');
            }

            if ($comments) {
                foreach ($comments as $comment) {
                    if ($comment) {
                        if (strtotime($comment->getEditdate()) > strtotime($comment->getDate()))
                            $lasttime = strtotime($comment->getEditdate());
                        else
                            $lasttime = strtotime($comment->getDate());
                        $raw_time_mess = $lasttime - time() + \Config::read('raw_time_mess');
                        if ($raw_time_mess <= 0) $raw_time_mess = false;

                        $comment->setRaw_time_mess($raw_time_mess);
                        $comment->setAvatar('<img class="ava" src="' . getAvatar($comment->getUser_id()) . '" alt="User avatar" title="' . h($comment->getName()) . '" />');

                        if ($comment->getUser_id()) {
                            $comment->setName_a(get_link(h($comment->getName()), getProfileUrl((int)$comment->getUser_id(), true)));
                            $comment->setUser_url(getProfileUrl((int)$comment->getUser_id()));
                        } else {
                            $comment->setName_a(h($comment->getName()));
                        }
                        $comment->setName(h($comment->getName()));

                        $comment->setMessage(\PrintText::print_page($comment->getMessage(), false, false, false, true));

                        if ( !is_null($comment->getEditdate()) ) {
                            $comment->setEditdate($comment->getEditdate());
                        } else {
                            $comment->setEditdate('');
                        }
                        if ($is_user) {
                            $comment->setEntry_url(get_url('/' . $comment->getModule() . '/view/' . $comment->getEntity_id()));
                        }
                    }
                }
            }
            $data = $comments;

        } else {
            $data = array();
        }
        return $data;
    }
}
