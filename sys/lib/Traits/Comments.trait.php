<?php
/**
* @project    Atom-M CMS
* @package    Commented Module Class
* @url        https://atom-m.modos189.ru
*/

namespace Traits;

trait Comments {
    use CommentsList;

    /**
     * add comment
     *
     * @id (int)    entity ID
     * @return      info message
     */
    public function add_comment($id = null) {
        \ACL::turnUser(array($this->module, 'view_list'),true);
        //turn access
        \ACL::turnUser(array($this->module, 'add_comments'),true);
        if (!empty($id) && !is_numeric($id))
            return $this->showMessage(__('Value must be numeric'));
        $id = (int)$id;
        if ($id < 1) return $this->showMessage(__('Unknown error'),getReferer(),'error', true);


        $target_new = $this->Model->getById($id);
        if (!$target_new) return $this->showMessage(__('Unknown error'),entryUrl($target_new, $this->module));
        if (!$target_new->getCommented()) return $this->showMessage(__('Comments are denied here'),entryUrl($target_new, $this->module));

        /* cut and trim values */
        if (\UserAuth::isUser()) {
            $name = \UserAuth::getField('name');
        } else {
            if (isset($_POST['login'])) {
                $name = mb_substr($_POST['login'], 0, 70);
                $name = trim($name);
            } else {
                $name = '';
            }
        }

        if (isset($_POST['reply']) and \Config::read('comments_tree'))
            $reply = intval($_POST['reply']);
        else
            $reply = 0;

        $mail = '';
        $message = trim($_POST['message']);
        $ip      = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
        $keystring = (isset($_POST['captcha_keystring'])) ? trim($_POST['captcha_keystring']) : '';


        // Check fields
        $error = '';

        // Check additional fields if an exists.
        // This must be doing after define $error variable.
        $addFields = $this->checkAdditionalFields('comments');
        if (is_string($addFields)) {
            $error .= $addFields;
        }

        $message = \PrintText::getPurifedHtml($message);

        $max_length = \Config::read($this->module, 'comment_length');
        if ($max_length <= 0)
            $max_length = 500;
        $min_length = 2;

        if (empty($name))
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Login')) . '</li>' . "\n";
        elseif (!\Validate::cha_val($name, V_TITLE))
            $error .= '<li>' . sprintf(__('Wrong chars in field "param"'), __('Login')) . '</li>' . "\n";
        if (empty($message))
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Text of comment')) . '</li>' . "\n";
        elseif (mb_strlen($message) > $max_length)
            $error .= '<li>' . sprintf(__('Very big "param"'), __('Text of comment'), $max_length) . '</li>' . "\n";
        elseif (mb_strlen($message) < $min_length)
            $error .= '<li>' . sprintf(__('Very small "param"'), __('Text of comment'), $min_length) . '</li>' . "\n";


        // Check captcha if need exists
        if (!\ACL::turnUser(array('__other__', 'no_captcha'))) {
            if (empty($keystring))
                $error .= '<li>' . sprintf(__('Empty field "param"'), __('Captcha')) . '</li>' . "\n";


            // Проверяем поле "код"
            if (!empty($keystring)) {
                // Проверяем поле "код" на недопустимые символы
                if (!\Validate::cha_val($keystring, V_CAPTCHA))
                    $error = $error.'<li>' . sprintf(__('Wrong chars in field "param"'), __('Captcha')) . '</li>'."\n";
                if (!isset($_SESSION['captcha_keystring'])) {
                    if (file_exists(ROOT . '/core/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat')) {
                        $_SESSION['captcha_keystring'] = file_get_contents(ROOT . '/core/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat');
                        @_unlink(ROOT . '/core/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat');
                    }
                }
                if (!isset($_SESSION['captcha_keystring']) || $_SESSION['captcha_keystring'] != $keystring)
                    $error = $error.'<li>' . __('Wrong protection code') . '</li>'."\n";
            }
            unset($_SESSION['captcha_keystring']);
        }

        /* if an errors */
        if (!empty($error)) {
            $_SESSION['addCommentForm'] = array();
            $_SESSION['addCommentForm']['errors'] = '<p class="errorMsg">' . __('Some error in form') . '</p>' .
                "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
            $_SESSION['addCommentForm']['name'] = $name;
            $_SESSION['addCommentForm']['message'] = $message;
            return $this->showMessage($_SESSION['addCommentForm']['errors'], entryUrl($target_new, $this->module));
        }


        $id_user = \UserAuth::getField('id');
        /* SPAM DEFENCE */
        if (isset($_SESSION['unix_last_post']) and (time()-$_SESSION['unix_last_post'] < 10)) {
            return $this->showMessage(__('You can not add messages so often'),entryUrl($target_new, $this->module));
        } else {
            $_SESSION['unix_last_post'] = time();
        }


        /* remove cache */
        if (!isset($this->Cache)) $this->Cache = new \Cache('pages');
        $this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module, 'record_id_' . $id));


        $commentsModel = \OrmManager::getModelInstance('Comments');
        if (!$commentsModel) return $this->showMessage(__('Some error occurred'),entryUrl($target_new, $this->module));

        if (\Config::read('comments_tree') and $reply != 0) {
            $parent_comment = $commentsModel->getFirst(array(
                    'id' => $reply,
                ), array());
            if (!$parent_comment)
                return $this->showMessage(__('Parent comment not found'),entryUrl($target_new, $this->module));
        }

        $prev_comm = $commentsModel->getFirst(array(
                'entity_id' => $id,
            ), array(
                'order' => 'date DESC, id DESC',
        ));

        $gluing = true;
        if ($prev_comm) {
            if (strtotime($prev_comm->getEditdate()) > strtotime($prev_comm->getDate()))
                $lasttime = strtotime($prev_comm->getEditdate());
            else
                $lasttime = strtotime($prev_comm->getDate());
            $gluing = $lasttime > time() - \Config::read('raw_time_mess');
            $prev_post_author = $prev_comm->getUser_id();
            if (empty($prev_post_author))
                $gluing = false;
            if ((mb_strlen($prev_comm->getMessage() . $message)) > $max_length)
                $gluing = false;
            if ($prev_post_author != $id_user || empty($id_user))
                $gluing = false;
            if (\Config::read('comments_tree') and $prev_comm->getParent_id() != $reply)
                $gluing = false;
        } else {
            $gluing = false;
        }


        if ($gluing === true) {
            $message = $prev_comm->getMessage() . "\n\n" . sprintf(__('Added in time'), AtmOffsetDate(strtotime($prev_comm->getDate()))) . "\n\n" . $message;

            $prev_comm->setMessage($message);
            $prev_comm->setEditdate((new \DateTime())->format('Y-m-d H:i:s'));
            $prev_comm->save();

            if ($this->isLogging) \Logination::write('adding comment to ' . $this->module, $this->module . ' id(*gluing)');
            return $this->showMessage(__('Comment is added'),entryUrl($target_new, $this->module),'ok');
        } else {
            /* save data */
            $data = array(
                'entity_id'   => $id,
                'parent_id'   => $reply,
                'name'     => $name,
                'message'  => $message,
                'ip'       => $ip,
                'user_id'  => $id_user,
                'date'     => (new \DateTime())->format('Y-m-d H:i:s'),
                'mail'     => $mail,
                'module'   => $this->module,
                'editdate' => new \Expr('NULL'),
                'premoder'     => 'confirmed',
            );

            // Save additional fields
            $data = array_merge($data, $addFields);

            if (\ACL::turnUser(array($this->module, 'comments_require_premoder'))) {
                $data['premoder'] = 'nochecked';
            }

            $className = \OrmManager::getEntityName('Comments');
            $entityComm = new $className($data);
            if ($entityComm) {
                $entity = $this->Model->getById($id);

                $event = \Events::init('before_add_comment', array(
                    'comment'=> $entityComm,
                    'entity' => $entity,
                    'module' => $this->module,
                ));

                if (count($event) >= 3 && isset($event['comment']) && isset($event['entity']) && isset($event['module']))
                {
                    $entityComm = $event['comment'];
                    $entity = $event['entity'];
                    $module = $event['module'];

                    if (isset($event['is_spam']) && $event['is_spam']) {
                        if ($this->isLogging)
                            \Logination::write('prevented spam comment to ' . $module, $module . ' id(' . $id . ')');
                        return $this->showMessage(__('Comment is added'), entryUrl($target_new, $module), 'ok');
                    }


                    $entityComm->setId($entityComm->save());
                    if ($entity) {
                        $entity->setComments($entity->getComments() + 1);
                        $entity->save();

                        // hook for plugins
                        \Events::init('new_comment', array(
                            'comment' => $entityComm,
                            'entity' => $entity,
                            'module' => $module,
                        ));

                        if ($this->isLogging)
                            \Logination::write('adding comment to ' . $module, $module . ' id(' . $id . ')');

                        if (\ACL::turnUser(array($module, 'comments_require_premoder')))
                            $msg = $this->showMessage(__('Comment will be available after validation'), entryUrl($target_new, $module), 'grand');
                        else
                            $msg = $this->showMessage(__('Comment is added'), entryUrl($target_new, $module), 'ok');
                        return $msg;
                    }
                }
            }
        }
        return $this->showMessage(__('Some error occurred'),entryUrl($target_new, $this->module));
    }

    /**
     * add comment form
     *
     * @id (int)    entity ID
     * @param null $id
     * @return array $data
     */
    protected function _add_comment_form($id = null) {
        //turn access
        \ACL::turnUser(array($this->module, 'add_comments'),true);

        $id = (int)$id;
        if ($id < 1) {
            $data = array();
        } else {
            $markers = ['access' => 1];

            // Additional fields
            $addFields = $this->getAdditionalFields('comments');
            foreach ($addFields as $k => $field) {
                $markers[strtolower($k)] = $field;
            }

            $name = (\UserAuth::isUser()) ? h(\UserAuth::getField('name')) : '';
            $message = '';
            $info = '';

            $markers['action'] = get_url($this->getModuleURL('/add_comment/' . $id));

            $kcaptcha = '';
            if (!\ACL::turnUser(array('__other__', 'no_captcha'))) {
                $kcaptcha = getCaptcha();
            }

            $markers['disabled'] = (\UserAuth::isUser()) ? ' disabled="disabled"' : '';
            $markers['add_comment_captcha'] = $kcaptcha;
            $markers['add_comment_name'] = $name;
            $markers['add_comment_message'] = $message;

            $data = $markers;
        }
        return $data;
    }

    /**
     * edit comment form
     *
     * @id (int)    comment ID
     * @return      html form
     */
    public function edit_comment_form($id = null) {
        \ACL::turnUser(array($this->module, 'view_list'),true);
        if (!empty($id) && !is_numeric($id))
            return $this->showMessage(__('Value must be numeric'));
        // проверка прав

        if (!\ACL::turnUser(array($this->module, 'edit_comments'))
            && (!\ACL::turnUser(array($this->module, 'edit_my_comments')))) {

            return $this->showMessage(__('Permission denied'),getReferer(),'error', true);
        }

        $id = (!empty($id)) ? (int)$id : 0;
        if ($id < 1) return $this->showMessage(__('Unknown error'),getReferer(),'error', true);


        $commentsModel = \OrmManager::getModelInstance('Comments');
        if (!$commentsModel) return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);
        $comment = $commentsModel->getById($id);
        if (!$comment) return $this->showMessage(__('Comment not found'),getReferer(),'error', true);

        if (count($comment) > 0) {
            $comment = $this->mergeAdditionalFields(array($comment), 'comments')[0];
        }

        if (strtotime($comment->getEditdate()) > strtotime($comment->getDate()))
            $lasttime = strtotime($comment->getEditdate());
         else
            $lasttime = strtotime($comment->getDate());
        $raw_time_mess = $lasttime - time() + \Config::read('raw_time_mess');
        if ($raw_time_mess <= 0) $raw_time_mess = false;

        // дополнительная проверка прав
        if (!\ACL::turnUser(array($this->module, 'edit_comments'))
            && (\ACL::turnUser(array($this->module, 'edit_my_comments'))
            && (\Config::read('raw_time_mess') == 0 or $raw_time_mess)) === false) {

            return $this->showMessage(__('Permission denied'),getReferer(),'error', true);
        }


        // Categories tree
        $entity = $this->Model->getById($comment->getEntity_id());
        $this->buildCategoriesMarker(($entity && $entity->getCategory_id()) ? $entity->getCategory_id() : false);

        $comment->setDisabled(($comment->getUser_id()) ? ' disabled="disabled"' : '');


        // Если при заполнении формы были допущены ошибки
        if (isset($_SESSION['editCommentForm'])) {
            $errors   = $_SESSION['editCommentForm']['errors'];
            $message  = $_SESSION['editCommentForm']['message'];
            $name     = $_SESSION['editCommentForm']['name'];
            unset($_SESSION['editCommentForm']);
        } else {
            $errors = '';
            $message = $comment->getMessage();
            $name    = $comment->getName();
        }


        $comment->setAction(get_url($this->getModuleURL('/update_comment/' . $id)));
        $comment->setErrors($errors);
        $comment->setName(h($name));
        $comment->setMessage(h($message));

        // nav block
        $navi = array();
        $navi['navigation'] = $this->buildBreadCrumbs(false, __('Comment editing'));
        $this->_globalize($navi);

        $source = $this->render('editcommentform.html.twig', array(
            'context' => $comment,
            'form' => &$comment,
        ));
        $this->comments = '';

        return $this->_view($source);
    }

    /**
     * update comment
     *
     * @id (int)    comment ID
     * @return      info message
     */
    public function update_comment($id = null) {
        \ACL::turnUser(array($this->module, 'view_list'),true);
        if (!empty($id) && !is_numeric($id))
            return $this->showMessage(__('Value must be numeric'));
        // проверка прав

        if (!\ACL::turnUser(array($this->module, 'edit_comments'))
            && (!\ACL::turnUser(array($this->module, 'edit_my_comments')))) {

            return $this->showMessage(__('Permission denied'),getReferer(),'error', true);
        }

        $id = (!empty($id)) ? (int)$id : 0;
        if ($id < 1) return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);


        $commentsModel = \OrmManager::getModelInstance('Comments');
        if (!$commentsModel) return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);
        $comment = $commentsModel->getById($id);
        if (!$comment) return $this->showMessage(__('Comment not found'),getReferer(),'error', true);

        $material = $this->Model->getById($comment->getEntity_id());

        if (strtotime($comment->getEditdate()) > strtotime($comment->getDate()))
            $lasttime = strtotime($comment->getEditdate());
         else
            $lasttime = strtotime($comment->getDate());
        $raw_time_mess = $lasttime - time() + \Config::read('raw_time_mess');
        if ($raw_time_mess <= 0) $raw_time_mess = false;

        // дополнительная проверка прав
        if (!\ACL::turnUser(array($this->module, 'edit_comments'))
            && (\UserAuth::isUser() && $post->getUser_id() == \UserAuth::getField('id')
            && \ACL::turnUser(array($this->module, 'edit_my_comments'))
            && (\Config::read('raw_time_mess') == 0 or $raw_time_mess)) === false) {

            return $this->showMessage(__('Permission denied'),getReferer(),'error', true);
        }


        /* cut and trim values */
        if ($comment->getUser_id() > 0) {
            $name = $comment->getName();
        } else {
            $name = mb_substr($_POST['login'], 0, 70);
            $name = trim($name);
        }


        $mail = '';
        $message = (!empty($_POST['message'])) ? $_POST['message'] : '';
        $message = trim($message);


        $error = '';

        // Check additional fields if an exists.
        // This must be doing after define $error variable.
        $addFields = $this->checkAdditionalFields('comments');
        if (is_string($addFields)) {
            $error .= $addFields;
        }

        $max_length = \Config::read('comment_length', $this->module);
        if ($max_length <= 0)
            $max_length = 500;
        $min_length = 2;

        if (empty($name))
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Login')) . '</li>' . "\n";
        elseif (!\Validate::cha_val($name, V_TITLE))
            $error .= '<li>' . sprintf(__('Wrong chars in field "param"'), __('Login')) . '</li>' . "\n";
        if (empty($message))
            $error .= '<li>' . sprintf(__('Empty field "param"'), __('Text of comment')) . '</li>' . "\n";
        elseif (mb_strlen($message) > $max_length)
            $error .= '<li>' . sprintf(__('Very big "param"'), __('Text of comment'), $max_length) . '</li>' . "\n";
        elseif (mb_strlen($message) < $min_length)
            $error .= '<li>' . sprintf(__('Very small "param"'), __('Text of comment'), $min_length) . '</li>' . "\n";

        /* if an error */
        if (!empty($error)) {
            $_SESSION['editCommentForm'] = array();
            $_SESSION['editCommentForm']['errors'] = '<p class="errorMsg">' . __('Some error in form') . '</p>'
                . "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
            $_SESSION['editCommentForm']['message'] = $message;
            $_SESSION['editCommentForm']['name'] = $name;
            return $this->showMessage($_SESSION['editCommentForm']['errors'], getReferer(),'error', true);
        }


        //remove cache
        if (!isset($this->Cache)) $this->Cache = new \Cache('pages');
        $this->Cache->clean(CACHE_MATCHING_TAG, array('module_' . $this->module, 'record_id_' . $comment->getEntity_id()));


        // Update comment
        $data = array(
            'message'  => $message,
            'editdate' => (new \DateTime())->format('Y-m-d H:i:s')
        );
        if ($name) $data['name'] = $name;

        // Save additional fields
        $data = array_merge($data, $addFields);

        $comment->set($data);
        $comment->save();

        if ($this->isLogging) \Logination::write('editing comment for ' . $this->module, $this->module . ' id(' . $comment->getEntity_id() . '), comment id(' . $id . ')');
        return $this->showMessage(__('Operation is successful'), entryUrl($material, $this->module),'ok');
    }

    /**
     * delete comment
     *
     * @id (int)    comment ID
     * @return      info message
     */
    public function delete_comment($id = null) {
        \ACL::turnUser(array($this->module, 'view_list'),true);
        //turn access
        \ACL::turnUser(array($this->module, 'delete_comments'),true);
        if (!empty($id) && !is_numeric($id))
            return $this->showMessage(__('Value must be numeric'));
        $id = (!empty($id)) ? (int)$id : 0;
        if ($id < 1) return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);

        $commentsModel = \OrmManager::getModelInstance('Comments');
        if ($commentsModel) {
            $comment = $commentsModel->getById($id);
            if ($comment) {
                $entityID = $comment->getEntity_id();
                $comment->delete();

                $entity = $this->Model->getById($entityID);
                if ($entity) {
                    $entity->setComments($entity->getComments() - 1);
                    $entity->save();

                    if ($this->isLogging) \Logination::write('delete comment for ' . $this->module, $this->module . ' id(' . $entityID . ')');
                    return $this->showMessage(__('Comment is deleted'), entryUrl($entity, $this->module),'ok');
                }
            }
        }
        return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);
    }

    /**
     * premoder comment
     *
     * @param int $id - comment ID
     * @param string $type - moder type
     * @return      info message
     */
    public function premoder_comment($id, $type) {
        \ACL::turnUser(array($this->module, 'view_list'),true);
        //turn access
        \ACL::turnUser(array($this->module, 'view_list'),true);
        \ACL::turnUser(array('__other__', 'can_premoder'),true);
        if (!empty($id) && !is_numeric($id))
            return $this->showMessage(__('Value must be numeric'));
        $id = (!empty($id)) ? (int)$id : 0;
        if ($id < 1) return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);

        if (!in_array((string)$type, $this->premoder_types))
            return $this->showMessage(__('Some error occurred'));

        $commentsModel = \OrmManager::getModelInstance('Comments');
        if ($commentsModel) {
            $comment = $commentsModel->getById($id);
            if ($comment) {
                $entityID = $comment->getEntity_id();
                $comment->setPremoder((string)$type);
                $comment->save();

                $entity = $this->Model->getById($entityID);
                if ($entity) {
                    return $this->showMessage(__('Operation is successful'), entryUrl($entity, $this->module),'ok');
                }
            }
        }
        return $this->showMessage(__('Some error occurred'),getReferer(),'error', true);
    }

}
