<?php

class CoreManager
{
    private static $errors = "";
    static $downloads_url;
    private static $tempPath;

    public function __construct()
    {
        self::$downloads_url = "https://bitbucket.org/api/2.0/repositories/atom-m/cms/downloads";
        self::$tempPath = R."core/tmp/";
    }

    public function isPrepare() {
        $status = ((file_exists(ROOT . "/core/" . ATOM_CORE_DIRNAME) &&
            is_link(ROOT . "/admin")) &&
            is_link(ROOT . "/modules") &&
            is_link(ROOT . "/sys"));
        return $status;
    }

    public function prepareCore() {
        $ok = false;
        if (!file_exists(ROOT . "/core/" . ATOM_CORE_DIRNAME)) {
            $ok = mkdir(ROOT . "/core/" . ATOM_CORE_DIRNAME, 0755);
            if ($ok)
                $ok = symlink(ROOT . "/core/" . ATOM_CORE_DIRNAME . "/", ROOT . "/core/snapshot_current");
        }

        if (!is_link(ROOT . "/admin") && $ok) {
            $ok = rename(ROOT . "/admin/", ROOT."/core/snapshot_current/admin/");
            if ($ok)
                $ok = symlink(ROOT . "/core/snapshot_current/admin/", ROOT . "/admin");
        }

        if (!is_link(ROOT . "/modules") && $ok) {
            $ok = rename(ROOT . "/modules/", ROOT."/core/snapshot_current/modules/");
            if ($ok)
                $ok = symlink(ROOT . "/core/snapshot_current/modules/", ROOT . "/modules");
        }

        if (!is_link(ROOT . "/sys") && $ok) {
            $ok = rename(ROOT . "/sys/", ROOT."/core/snapshot_current/sys/");
            if ($ok)
                $ok = symlink(ROOT . "/core/snapshot_current/sys/", ROOT . "/sys");
        }
        return $ok;
    }

    public function getLocalCores()
    {
        $dirs = scandir(ROOT . "/core");
        $arr = array();
        foreach ($dirs as $dir) {
            if (
                (substr($dir, 0, 10) == "snapshot_v") and
                is_dir(ROOT . "/core/" . $dir)
            ) {
                $arr[$dir] = array('active' => false);
            }
        }
        $arr[ATOM_CORE_DIRNAME]['active'] = true;
        krsort($arr);
        return $arr;
    }

    public function getCores()
    {
        $Cache = new \Cache;
        $Cache->lifeTime = 6000;
        if ($Cache->check('atom-m-versions')) {
            $downloads_list_raw = $Cache->read('atom-m-versions');
        } else {
            $downloads_list_raw = file_get_contents(self::$downloads_url);
            $Cache->write($downloads_list_raw, 'atom-m-versions', array());
        }
        $downloads_list = json_decode($downloads_list_raw, true);

        $arr = array();

        // Если удалось получить записи
        if (is_array($downloads_list) and is_array($downloads_list['values'])) {

            foreach ($downloads_list['values'] as $item) {
                $version = explode("-", $item['name'])[2];
                if (($version < "8.1") or (!is_numeric($version))) continue;

                $snapshot = "snapshot_v" . str_replace(".", "_", $version);

                $arr[$snapshot] = array('filename' => $item['name'], 'created_on' => $item['created_on'], 'active' => false, 'installed' => false);
            }

        }

        $dirs = scandir(ROOT . "/core");
        foreach ($dirs as $dir) {
            if (
                (substr($dir, 0, 10) == "snapshot_v") and
                is_dir(ROOT . "/core/" . $dir)
            ) {
                if (isset($arr[$dir])) {
                    $arr[$dir] = array_merge($arr[$dir], array('active' => false, 'installed' => true));
                } else {
                    $arr[$dir] = array('active' => false, 'installed' => true);
                }
            }
        }

        $arr[ATOM_CORE_DIRNAME]['active'] = true;
        krsort($arr);
        return $arr;
    }

    public function installCore($filename) {
        self::$errors = '';
        $url = "https://bitbucket.org/!api/2.0/repositories/atom-m/cms/downloads/" . $filename;
        $version = explode("-", $filename)[2];
        $version = str_replace(".", "_", $version);

        if (copy($url, self::$tempPath . $filename)) {
            return Zip::extractZip(self::$tempPath . $filename, ROOT . "/core/snapshot_v" . $version);
        } else {
            self::$errors = __('Some error occurred');
            return false;
        }
    }

    public function selectCore($name) {
        if (is_dir(ROOT . "/core/" . $name)) {
            unlink(ROOT . "/core/snapshot_current");
            symlink(ROOT . "/core/" . $name . "/", ROOT . "/core/snapshot_current");
            return true;
        } else {
            return false;
        }
    }

    public function deleteCore($name) {
        if (is_dir(ROOT . "/core/" . $name) and (ATOM_CORE_DIRNAME != $name)) {
            return _unlink(ROOT . "/core/" . $name . "/");
        } else {
            return false;
        }
    }
}
