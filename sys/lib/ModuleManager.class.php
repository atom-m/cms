<?php
/**
* @project    Atom-M CMS
* @package    ModuleManager Class
* @url        https://atom-m.modos189.ru
*/
class ModuleManager
{
    private static $modulesPath ='modules';
    private static $configPath ='data/config';

    // Обязательные файлы
    private static $actionsFile = 'ActionsHandler.class.php';
    private static $eventsFile = 'EventsHandler.class.php';
    private static $configFile = 'config.php';
    // Необязательные файлы
    private static $settingsFile = 'settings.php';
    private static $templateFile = 'template_parts.php';
    private static $rulesFile = 'acl_rules.php';
    private static $menuFile = 'menu.php';
    private static $installFile = 'install.php';
    private static $uninstallFile = 'uninstall.php';

    /**
     *
     */
    public function __construct() {
    }

    /** Путь к каталогу модуля */
    public static function getModulePath($module) {
        return R . self::$modulesPath . DS . $module . DS;
    }

    /** Проверяет на существование модуль */
    public static function checkModule($module) {
        $module_path = self::getModulePath($module);
        return ((file_exists($module_path . self::$actionsFile) || file_exists($module_path . self::$eventsFile)) && file_exists($module_path . self::$configFile));
    }


    /** Возвращает массив неустановленных, но готовых к установке модулей */
    public static function getNewModules() {
        $new_modules = array();

        $module_paths = glob(R . self::$modulesPath . DS . '*', GLOB_ONLYDIR);
        foreach ($module_paths as $module_path) {
            $module = basename($module_path);
            if (!in_array($module, $new_modules) && self::checkModule($module) && !self::checkInstallModule($module)) {
                $new_modules[] = $module;
            }
        }
        return $new_modules;
    }


    /** Возвращает массив установленных модулей */
    public static function getInstalledModules() {
        return \Config::read('installed_modules');
    }


    /** Сохраняет массив установленных модулей */
    public static function setInstalledModules($installed_modules) {
        $config = \Config::read('all');
        $config['installed_modules'] = $installed_modules;
        \Config::write($config);
        
        clearstatcache();
        if (function_exists('opcache_reset')) {
            opcache_reset();
        }
    }


    /** Проверяет является ли модуль служебным */
    public static function checkServiceModule($module) {
        $service_modules = array('home');
        return (in_array($module, $service_modules));
    }


    /** Проверяет был ли установлен модуль */
    public static function checkInstallModule($module) {
        $installed_modules = self::getInstalledModules();
        return (is_array($installed_modules) && in_array($module, $installed_modules));
    }


    /** Устанавливает модуль */
    public static function installModule($module) {
        // Если модуль существует и является модулем
        if (self::checkModule($module)) {
            $customInstallFile = self::getModulePath($module) . self::$installFile;
            $out = (file_exists($customInstallFile) ? include($customInstallFile) : true);
            if (!empty($out) && $out != false) {
                $installed_modules = self::getInstalledModules();
                if (is_array($installed_modules)) {
                    $installed_modules[] = $module;
                    self::setInstalledModules(array_unique($installed_modules));
                }
                /*
                self::copyConfig($module);
                 */
            }
        }
    }

    /** Устанавливает модулю статус установленного */
    private static function copyConfig($module) {
        $configFile = self::getModulePath($module) . self::$configFile;
        if (file_exists($configFile)) {
            $moduleConfig = include($configFile);
            if (!empty($moduleConfig) && is_array($moduleConfig)) {
                \Config::write($moduleConfig, $module);
            }
        }
    }

    /** Удаляет модуль */
    public static function uninstallModule($module) {
        // Если модуль существует и является модулем
        if (self::checkInstallModule($module)) {
            $customUninstallFile = self::getModulePath($module) . self::$uninstallFile;
            $out = (file_exists($customUninstallFile) ? include($customUninstallFile) : true);
            if (!empty($out) && $out != false) {
                $installed_modules = self::getInstalledModules();
                if (is_array($installed_modules)) {
                    foreach (array_keys($installed_modules, $module) as $key) {
                        unset($installed_modules[$key]);
                    }
                    self::setInstalledModules(array_unique($installed_modules));
                }
                /*
                self::backupConfig($module);
                if (file_exists(R . "data/config/$module.php")) {
                    unlink(R . "data/config/$module.php");
                }
                 */
            }
        }
    }

    /** Создает резервную копию текущей конфигурации модуля */
    private static function backupConfig($module) {
        $moduleConfig = \Config::read('all', $module);
        $configFile = self::getModulePath($module) . self::$configFile;
        if ($fopen=@fopen($configFile, 'w')) {
            $data = '<?php ' . "\n" . 'return ' . var_export($moduleConfig, true) . "\n" . '?>';
            fputs($fopen, $data);
            fclose($fopen);
            return true;
        }
    }

    /** Получает список шаблонов необходимых для работы модуля */
    public static function getTemplateParts($module) {
        $templateFile = self::getModulePath($module) . self::$templateFile;
        if (file_exists($templateFile)) {
            $templateParts = include $templateFile;
            return (!empty($templateParts) ? $templateParts : false);
        }
        return false;
    }

    /** Получает список пунктов меню админ-панели для модуля */
    public static function getMenu($module) {
        $menuFile = self::getModulePath($module) . self::$menuFile;
        if (file_exists($menuFile)) {
            $menu = include $menuFile;
            return (!empty($menu) ? $menu : false);
        }
        return false;
    }

    /** Возвращает путь к файлу визуализации настроек модуля */
    public static function getSettingsFile($module) {
        return self::getModulePath($module) . self::$settingsFile;
    }

    /** Возвращает путь к правам доступа модуля */
    public static function getRulesFile($module) {
        return self::getModulePath($module) . self::$rulesFile;
    }
}