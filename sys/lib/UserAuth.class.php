<?php


/**
 *
 */
class UserAuth
{
    static private $default_user = array(
        'id' => 0,
        'status' => 0,
    );


    /**
     * @return void
     */
    static function setTimeVisit()
    {
        if (\UserAuth::isUser()) {
            $DB = getDB();

            $query = "UPDATE `" . $DB->getFullTableName('users') . "`
                    SET last_visit=NOW()
                    WHERE id=" . self::getField('id');
            $DB->query( $query );
        }
    }



    /**
     * @return bool|void
     */
    static function autoLogin()
    {
        $DB = getDB();


        // Если не установлены cookie, содержащие логин и пароль
        if ( !isset( $_COOKIE['userid'] ) or !isset( $_COOKIE['password'] ) ) {
            $path ='/';
            if ( isset( $_COOKIE['userid'] ) ) setcookie( 'userid', '', time() - 1, $path );
            if ( isset( $_COOKIE['password'] ) ) setcookie( 'password', '', time() - 1, $path );
            if ( isset( $_COOKIE['autologin'] ) ) setcookie( 'autologin', '', time() - 1, $path );
            return false;
        }
        // Проверяем переменные cookie на недопустимые символы
        $user_id = intval($_COOKIE['userid']);
        if ($user_id < 1) return false;
        $password = trim($_COOKIE['password']);


        // Выполняем запрос на получение данных пользователя из БД
        $res = $DB->select('users', DB_FIRST, array(
            'cond' => array(
                'id' => $user_id,
                'passw' => $password,
            ),
            'fields' => array(
                '*',
                'UNIX_TIMESTAMP(last_visit) as unix_last_visit',
            ),
        ));


        // Если пользователь с таким логином и паролем не найден -
        // значит данные неверные и надо их удалить
        if ( count( $res ) < 1 ) {
            //$tmppos = strrpos( $_SERVER['PHP_SELF'], '/' ) + 1;
            //$path = substr( $_SERVER['PHP_SELF'], 0, $tmppos );
            $path = '/';
            setcookie( 'autologin', '', time() - 1, $path );
            setcookie( 'userid', '', time() - 1, $path );
            setcookie( 'password', '', time() - 1, $path );
            return false;
        }


        $user = $res[0];
        if ( !empty( $user['activation'] ) ) {
            //$tmppos = strrpos( $_SERVER['PHP_SELF'], '/' ) + 1;
            //$path = substr( $_SERVER['PHP_SELF'], 0, $tmppos );
            $path = '/';
            setcookie( 'autologin', '', time() - 1, $path );
            setcookie( 'userid', '', time() - 1, $path );
            setcookie( 'password', '', time() - 1, $path );

            header( 'Refresh: ' . \Config::read('redirect_delay') . '; url=' . (used_https() ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . '/');
            $View = new Viewer_Manager();
            $output = $View->view('infomessagegrand.html.twig', array('data' => array('info_message' => __('Your account not activated',true,'users'), 'error_message' => null)));
            echo $output;
            die();
        }

        // Если пользователь заблокирован
        if ( $user['locked'] ) {
            //$tmppos = strrpos( $_SERVER['PHP_SELF'], '/' ) + 1;
            //$path = substr( $_SERVER['PHP_SELF'], 0, $tmppos );
            $path = '/';
            setcookie( 'autologin', '', time() - 1, $path );
            setcookie( 'userid', '', time() - 1, $path );
            setcookie( 'password', '', time() - 1, $path );
            redirect('/users/baned/');
        }

        self::setUser($user);
        self::autoTransfer($user);

        // Функция getNewThemes() помещает в массив $_SESSION['newThemes'] ID тем,
        // в которых были новые сообщения со времени последнего посещения пользователя
        self::getNewThemes();

        return true;
    }



    static function getNewThemes()
    {
        if (\UserAuth::isUser()) {
            $DB = getDB();

            $query = "SELECT a.id, MAX(UNIX_TIMESTAMP(b.time)) AS unix_last_post
                FROM `" . $DB->getFullTableName('themes') . "` a
                INNER JOIN `" . $DB->getFullTableName('posts') . "` b
                ON a.id=b.id_theme
                GROUP BY a.id
                HAVING unix_last_post>" . self::getField('unix_last_visit');

            $res = $DB->query($query);

            if ($res) {
                foreach ($res as $key => $row) {
                    $_SESSION['newThemes'][$row['id']] = $row['unix_last_post'];
                }
            }
        }
    }



    static function countNewMessages() {
        if (\UserAuth::isUser()) {
            $DB = getDB();

            $res = $DB->query("SELECT COUNT(*) as cnt
                    FROM `" . $DB->getFullTableName('messages') . "`
                    WHERE to_user=".(int)self::getField('id')."
                    AND viewed=0 AND id_rmv<>".(int)self::getField('id'));
            if ( $res ) {
                return $res[0]['cnt'];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Возвращает данные текущего пользователя
     *
     * @return array
     */
    static function getUser() {
        return (isset($_SESSION['user']) ? $_SESSION['user'] : self::$default_user);
    }

    /**
     * Возвращает данные из указанного поля текущего пользователя
     *
     * @param string $field
     * @return mixed
     */
    static function getField($field) {
        $aUser = self::getUser();
        return (isset($aUser[$field]) ? $aUser[$field] : '');
    }
    
    /**
     * Сохранить данные текущего пользователя в сессию
     *
     * @param array $user
     */
    static function setUser($user) {
        $_SESSION['user'] = $user;
    }

    /**
     * Сохранить данные в указанное поле текущего пользователя в сессию
     *
     * @param string $field
     * @param mixed $value
     */
    static function setField($field, $value) {
        $_SESSION['user'][$field] = $value;
    }

    /**
     * Очистить данные текущего пользователя
     *
     * @return array
     */
    static function clearUser() {
        unset($_SESSION['user']);
    }

    /**
     * Проверка является ли текущий пользователь гостем
     *
     * @return boolean
     */
    static function isGuest() {
        return (self::getField('id') == 0);
    }

    /**
     * Проверка авторизован ли текущий пользователь
     *
     * @return boolean
     */
    static function isUser() {
        return (self::getField('id') > 0);
    }
    
    /**
     * Проверка необходимости авто-переноса
     *
     * @param array $user
     */
    static function autoTransfer($user) {
        $types = array(
            'posts' => array('module' => 'forum', 'name' => 'posts'),
            'comments' => array('module' => 'comments'),
            'news' => array('module' => 'news'),
            'stats' => array('module' => 'stat'),
            'loads' => array('module' => 'loads'),
            'photo' => array('module' => 'foto'),
        );
        $acl_groups = \ACL::getGroups();

        if (isset($user['status']) && isset($acl_groups[$user['status']])) {
            $group = $acl_groups[$user['status']];
            if (isset($group['select']) && $group['select'] && isset($group['transfer']) && isset($group['transfer']) &&
                    ($to_group = isset($group['transfer']['to_group']) && intval($group['transfer']['to_group']) > 0 ? intval($group['transfer']['to_group']) : 0) > 0) {
                $logic = isset($group['transfer']['logic']) && intval($group['transfer']['logic']) != 0 ? intval($group['transfer']['logic']) : 0;
                
                $usersModel = \OrmManager::getModelInstance('Users');
                $stat = $usersModel->getFullUserStatistic($user['id']);
                
                $values = array();
                if (isset($group['transfer']['days']) && intval($group['transfer']['days']) > 0) {
                    $values['days'] = array(
                        'need' => intval($group['transfer']['days']),
                        'current' => (time() - strtotime($user['puttime'])) / 60 / 60 / 24,
                    );
                }
                foreach ($types as $type => $cond) {
                    if (isset($group['transfer'][$type]) && intval($group['transfer'][$type]) > 0) {
                        $need = intval($group['transfer'][$type]);
                        foreach ($stat as $st) {
                            if ($st['module'] == $cond['module'] && (!isset($cond['name']) || (isset($cond['name']) && $st['name'] == $cond['name']))) {
                                $current = isset($st['count']) ? $st['count'] : 0;
                                break;
                            }
                        }
                        $values[$type] = array(
                            'need' => $need,
                            'current' => $need,
                        );
                    }
                }
                
                $result = false;
                if ($logic == 0) { // Logic AND
                    $result = true;
                    foreach ($values as $value) {
                        $result &= $value['current'] >= $value['need'];
                    }
                } else { // Logic OR
                    $result = false;
                    foreach ($values as $value) {
                        $result |= $value['current'] >= $value['need'];
                    }
                }
                
                if ($result) {
                    $DB = getDB();
                    $DB->query("UPDATE `" . $DB->getFullTableName('users') . "` SET `status` = " . $to_group . " WHERE `id` = '" . $user['id'] . "'");
                    if (\Config::read('__secure__.system_log')) {
                        \Logination::write('auto-transfer user', 'user id(' . $user['id'] . '), group (' . $user['status'] . ' -> ' . $to_group . ')');
                    }
                    $user['status'] = $to_group;
                    self::setUser($user);
                }
            }
        }
    }
}
