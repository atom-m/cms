<?php

if (!isset($usec) && !isset($sec))
    list($usec, $sec) = explode(" ", microtime());

session_start();
if (isset($_SESSION['db_querys'])) unset($_SESSION['db_querys']);

// Назначение дефолтной временной зоны, чтобы подсчеты велись так как надо.
date_default_timezone_set('UTC');

/**
 * Current version of engine
 */
define('ATOM_VERSION', '9.0');

define('ATOM_CORE_DIRNAME', 'snapshot_v'.str_replace(".", "_", ATOM_VERSION));

/**
 * Path constants
 */
define ('DS', DIRECTORY_SEPARATOR);

// Определение, sys в корневой папке или core/snapshot_vVERSION/
if (strpos(realpath(__DIR__), 'core'.DS.ATOM_CORE_DIRNAME) !== false) {
    define('ROOT', dirname(dirname(dirname(realpath(__DIR__)))));
} else {
    define('ROOT', dirname(realpath(__DIR__)));
}

define ('R', ROOT . DS);


/**
 * If we uses CMS from subdir or subdirs
 * we must set this variable, because CMS
 * must know this for good work.
 */
define ('WWW_ROOT', str_replace('\\', '/', mb_substr(ROOT , mb_strlen(realpath($_SERVER['DOCUMENT_ROOT']), 'utf-8'))));

/**
 * If set to 1, check referer in admin panel
 * and if he dont match current host redirect to
 * index page of admin panel. It doesn't allow to
 * send inquiries from other hosts.
 */
define ('ADM_REFER_PROTECTED', 0);






/**
 * whether the system is installed
 */
function isInstall() {
    return !file_exists(ROOT . '/install');
}



/**
 * Autoload
 */
// Загружает класс, если он еще не был загружен при его непосредственном вызове
include_once 'lib/AL.class.php';
AL::register();


function loadFuncs() {
    $files = glob(ROOT . '/sys/fnc/*.php');
    if (count($files)) {
        foreach ($files as $file) {
            include_once $file;
        }
    }
}
loadFuncs();


/**
 * Registry
 */
$Register = Register::getInstance();
/**  TOUCH START TIME */
$Register['atm_boot_start_time'] = ((float)$usec + (float)$sec);
/** /TOUCH START TIME */

$Register['Config'] = new Config(ROOT . '/sys/settings/config.php');


// Adding standart namespace locations
\AL::addNamespace("HomeModule", array(R.'modules/home')); // Default class
$installed_modules = \ModuleManager::getInstalledModules();
if (!empty($installed_modules)) {
    foreach($installed_modules as $module) {
        \AL::addNamespace(ucfirst($module)."Module", array(R.'modules/'.$module));
    }
}


/**  SET PHP SETTINGS */
@ini_set('session.gc_maxlifetime', 10000);
ini_set('post_max_size', "100M");
ini_set('upload_max_filesize', "100M");
if (function_exists('set_time_limit')) @set_time_limit(200);
ini_set('register_globals', 0);
ini_set('magic_quotes_gpc', 0);
ini_set('magic_quotes_runtime', 0);
session_set_cookie_params(3000);
ini_set('display_errors', 0);
error_reporting(E_ALL & ~E_NOTICE);

/** if debug mode On - view errors */
if (\Config::read('debug_mode') == 1) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('log_errors', 1);
ini_set('error_log', ROOT . '/core/logs/php_errors.log');

/**
 * Set default encoding
 * After set this, we mustn't set encoding
 * into next functions: mb_substr, mb_strlen, etc...
 */
if (function_exists('mb_internal_encoding'))
    mb_internal_encoding('UTF-8');

/** /SET PHP SETTINGS */



/** FINAL LOADING */
if (isInstall()) {

    if (get_magic_quotes_gpc()) {
        strips($_GET);
        strips($_POST);
        strips($_COOKIE);
        strips($_REQUEST);
        if (isset($_SERVER['PHP_AUTH_USER'])) strips($_SERVER['PHP_AUTH_USER']);
        if (isset($_SERVER['PHP_AUTH_PW'])) strips($_SERVER['PHP_AUTH_PW']);
    }
    
    /** Secure checking */
    Protect::checkIpBan();
    if (\Config::read('anti_ddos', '__secure__') == 1) Protect::antiDdos();
    
    
    if (isGuest()) {
        /*
        * Auto login... if user during a previos
        * visit set AUTOLOGIN option
        */
        if (isset($_COOKIE['autologin']))
            UserAuth::autoLogin();
    
        /*
        * if user is autorizet set
        * last time visit
        * This information store in <DataBase>.users
        */
        UserAuth::setTimeVisit();
    }

}




