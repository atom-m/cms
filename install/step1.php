<?php

if (basename(dirname(__FILE__)) != "install") {
    header('Location: /');
    die();
}

include_once '../sys/boot.php';

if (isset($_POST['send'])) {
    include_once 'check_sql_server.php';

    if (empty($_POST['host'])) $errors['db_host'] = 'Не заполненно поле "Хост базы данных"';
    if (empty($_POST['base'])) $errors['db_name'] = 'Не заполненно поле "Имя базы данных"';
    if (empty($_POST['user'])) $errors['db_user'] = 'Не заполненно поле "Логин"';
    
    if (empty($_POST['adm_login'])) $errors['adm_login'] = 'Не заполненно поле "Логин"';
    if (empty($_POST['adm_pass'])) $errors['adm_pass'] = 'Не заполненно поле "Пароль"';

    $errors = [];
    if (check_sql_server($_POST['host'], $_POST['user'], $_POST['pass'], $_POST['base']) !== true) {
        $errors['connect'] = 'Не удалось подключиться к базе. Проверьте настройки!';
    }
    
    if (!empty($_POST['prefix']) && !preg_match('#^[a-z_]*$#i', $_POST['prefix'])) $errors['db_prefix'] = 'Не допустимые символы в поле "Префикс"';

    $adm_login = mb_substr($_POST['adm_login'], 0, 30);
    $adm_pass = mb_substr($_POST['adm_pass'], 0, 64);

    if (!empty($adm_login) && !\Validate::cha_val($adm_login, V_LOGIN))
        $errors['login'] = '<li>' . sprintf(__('Wrong chars in field "param"'), __('login')) . '</li>' . "\n";

    if (empty($errors)) {
        $settings = \Config::read('all');

        $settings['__db__'] = array();
        $settings['__db__']['host']   = $_POST['host'];
        $settings['__db__']['name']   = $_POST['base'];
        $settings['__db__']['user']   = $_POST['user'];
        $settings['__db__']['pass']   = $_POST['pass'];
        $settings['__db__']['prefix'] = $_POST['prefix'];

        Config::write($settings);
        
        $_SESSION['prefix'] = $_POST['prefix'];
        $_SESSION['adm_name'] = $adm_login;
        $_SESSION['adm_pass'] = $adm_pass;
        header ('Location: step2.php '); die();
    }    
}

$CM = new \CoreManager;
$CM->prepareCore();

$Viewer = new \Viewer_Manager(['template_path' => ROOT . '/install/template/html/', 'layout' => false]);
$output = $Viewer->parseTemplate('step1.html.twig', array());

echo($output);
