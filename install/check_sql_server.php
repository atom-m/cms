<?php
@ini_set('display_errors', 0);

function check_sql_server($host, $user, $pass, $base=null, $echo=false) {
    $db = new mysqli($host, $user, $pass);

    // проверяем соединение 
    if (mysqli_connect_errno()) {
        if ($echo) {
            echo '<span style="color:#FF0000">Не удалось подключиться</span>';
        }
        return false;
    }
    
    $status = false;
    if ($base == null) {
        if ($echo) {
            echo '<span style="color:#46B100">Подключились</span>';
        }
        $status = true;
    } else {
        if ($db->select_db($base) === true) {
            if ($echo) {
                echo '<span style="color:#46B100">База найдена</span>';
            }
            $status = true;
        } else {
            if ($echo) {
                echo '<span style="color:#FF0000">Не удалось найти базу</span>';
            }
        }
    }

    $db->close();
    return $status;
}

if (isset($_GET['host']) and isset($_GET['user']) and isset($_GET['pass'])) {
    if (empty($_GET['base'])) $_GET['base'] = null;
    check_sql_server($_GET['host'], $_GET['user'], $_GET['pass'], $_GET['base'], true);
}
