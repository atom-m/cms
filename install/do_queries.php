<?php
/**
* @project    Atom-M CMS
* @package    Install
* @url        https://atom-m.modos189.ru
*/

@ini_set('display_errors', 1);
if (function_exists('set_time_limit')) set_time_limit(0);
include_once '../sys/boot.php';

$errors = array();

$DB = getDB();

$array = array();
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}forum_cat`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}forum_cat` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) collate utf8_general_ci default NULL,
  `previev_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}forum_cat` VALUES (1, 'TEST', 1)";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}forums`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}forums` (
  `id` int(6) NOT NULL auto_increment,
  `title` text character set utf8,
  `description` mediumtext character set utf8,
  `pos` smallint(6) NOT NULL default '0',
  `in_cat` int(11) default NULL,
  `last_theme_id` int(11) NOT NULL default 0,
  `themes` int(11) default '0',
  `posts` int(11) NOT NULL default '0',
  `parent_forum_id` INT(11),
  `lock_posts` INT( 11 ) DEFAULT '0' NOT NULL,
  `lock_passwd` VARCHAR( 100 ) NOT NULL default 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}forums` (`title`, `description`, `pos`, `in_cat`) VALUES ('TEST', 'тестовый форум', 1, 1)";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}pages`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}pages` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) collate utf8_general_ci NOT NULL,
  `title` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `template` varchar(255) default '' collate utf8_general_ci NOT NULL,
  `content` longtext collate utf8_general_ci NOT NULL,
  `url` varchar(255) default ''  NOT NULL,
  `meta_title` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `meta_keywords` varchar(255) default ''  NOT NULL,
  `meta_description` text,
  `parent_id` int(11) default 0  NOT NULL,
  `path` varchar(255) default '1.'  NOT NULL,
  `position` INT( 11 ) NOT NULL,
  `publish` ENUM( '0', '1' ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}pages` (`id`,`name`,`path`,`content`) VALUES ('1', 'root', '.', '')";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}loads`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}loads` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `main` text NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` VARCHAR( 255 ) NOT NULL,
  `views` int(11) default '0',
  `downloads` int(11) default '0',
  `rate` int(11) default '0',
  `download` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `download_url` VARCHAR( 255 ) NOT NULL,
  `download_url_size` bigint( 20 ) NOT NULL,
  `date` datetime NULL default NULL,
  `comments` int(11) NOT NULL default '0',
  `tags` VARCHAR( 255 ) NOT NULL,
  `description` TEXT NOT NULL,
  `sourse` VARCHAR( 255 ) NOT NULL,
  `commented` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `available` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `on_home_top` ENUM( '0', '1' ) DEFAULT '0' NOT NULL,
  `premoder` ENUM( 'nochecked', 'rejected', 'confirmed' ) NOT NULL DEFAULT 'confirmed',
  `hlu` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}comments`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}comments` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) DEFAULT '0' NOT NULL,
  `entity_id` int(11) NOT NULL,
  `user_id` INT(11) DEFAULT '0' NOT NULL,
  `name` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `ip` varchar(50) NOT NULL,
  `mail` varchar(150) NOT NULL,
  `date` DATETIME NULL default NULL,
  `editdate` DATETIME NULL default NULL,
  `module` varchar(10) default 'news' NOT NULL,
  `premoder` enum('nochecked','rejected','confirmed') NOT NULL DEFAULT 'nochecked',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}loads_categories`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}loads_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) default '0',
  `announce` varchar(255) NOT NULL default '',
  `title` varchar(255) NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `no_access` VARCHAR( 255 ) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}loads_categories` VALUES (1, 0, '', 'TEST CAT', '1', '')";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}messages`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}messages` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `to_user` int(10) unsigned NOT NULL default '0',
  `from_user` int(10) unsigned NOT NULL default '0',
  `sendtime` datetime NULL default NULL,
  `subject` varchar(255) default NULL,
  `message` text,
  `id_rmv` int(10) unsigned NOT NULL default '0',
  `viewed` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}news`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}news` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) collate utf8_general_ci NOT NULL,
  `main` text collate utf8_general_ci NOT NULL,
  `views` int(11) default '0',
  `date` datetime NULL default NULL,
  `category_id` VARCHAR( 255 ) NOT NULL,
  `author_id` int(11) NOT NULL,
  `comments` int(11) NOT NULL default '0',
  `tags` VARCHAR( 255 ) NOT NULL,
  `description` TEXT NOT NULL,
  `sourse` VARCHAR( 255 ) NOT NULL,
  `commented` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `available` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `on_home_top` ENUM( '0', '1' ) DEFAULT '0' NOT NULL,
  `premoder` ENUM( 'nochecked', 'rejected', 'confirmed' ) NOT NULL DEFAULT 'confirmed',
  `hlu` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}news` VALUES (1, 'Моя первая новость', '
[imgr]".(used_https() ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].WWW_ROOT."/data/img/logo.png[/imgr]

Теперь сайт установлен и готов к работе.
Подписывайтесь, задавайте вопросы и получайте полезную информацию в наших группах:

[b][!url=https://twitter.com/AtomM_CMS]Twitter[/url]
[!url=https://plus.google.com/+AtomMnetCMS]Google+[/url]
[!url=https://vk.com/atom_m]Вконтакте[/url][/b]

[b]Несколько советов:[/b]
[b]1.[/b] Документацию Вы можете найти в нашей [!url=https://bitbucket.org/atom-m/cms/wiki]Wiki[/url].
[b]2.[/b] Большинство настроек можно задать в разделе [b]Общие настройки[/b] в админке и индивидуальных настройках каждого модуля.
[b]3.[/b] Не забудьте настроить права доступа. Для этого перейдите на страницу [b]Админка - Безопасность - Редактор прав[/b].
[b]4.[/b] Вы можете сделать любую страницу сайта главной. Это настраивается в общих настройках.
[b]5.[/b] Если Вы хотите сообщить об ошибке, это можно сделать [!url=https://bitbucket.org/atom-m/cms/issues]тут[/url].
', 0, NOW(), 1, 1, 0, '', '', '', '1', '1', '1', '0', 'confirmed', 'moya-pervaya-novost')";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}news_categories`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}news_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) default '0',
  `announce` varchar(255) NOT NULL default '',
  `title` varchar(255) NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `no_access` VARCHAR( 255 ) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}news_categories` VALUES (1, 0, '', 'Test category', '1', '')";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}posts`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}posts` (
  `id` int(11) NOT NULL auto_increment,
  `message` text,
  `id_author` int(6) unsigned NOT NULL default '0',
  `time` datetime NULL default NULL,
  `edittime` datetime NULL default NULL,
  `id_editor` int(6) unsigned NOT NULL default '0',
  `id_theme` int(11) NOT NULL default '0',
  `id_forum` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}stat`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}stat` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `main` longtext NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` VARCHAR( 255 ) NOT NULL,
  `views` int(11) default '0',
  `rate` int(11) default '0',
  `date` datetime NULL default NULL,
  `comments` int(11) NOT NULL default '0',
  `tags` VARCHAR( 255 ) NOT NULL,
  `description` TEXT NOT NULL,
  `sourse` VARCHAR( 255 ) NOT NULL,
  `commented` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `available` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `on_home_top` ENUM( '0', '1' ) DEFAULT '0' NOT NULL,
  `premoder` ENUM( 'nochecked', 'rejected', 'confirmed' ) NOT NULL DEFAULT 'confirmed',
  `hlu` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}stat_categories`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}stat_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) default '0',
  `announce` varchar(255) NOT NULL default '',
  `title` varchar(255) NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `no_access` VARCHAR( 255 ) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}stat_categories` VALUES (1, '0', '', 'Test category', '1', '')";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}statistics`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}statistics` (
  `id` int(11) NOT NULL auto_increment,
  `visits` int(50) default '1',
  `date` date default NULL,
  `views` int(11) NOT NULL,
  `other_site_visits` int(11) default '0',
  `bot_views` BLOB NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8  COLLATE=utf8_general_ci;";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}themes`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}themes` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(120) default NULL,
  `id_author` int(6) NOT NULL default '0',
  `time` datetime NULL default NULL,
  `id_last_author` int(6) NOT NULL default '0',
  `last_post` datetime NULL default NULL,
  `id_forum` int(2) NOT NULL default '0',
  `locked` tinyint(1) unsigned NOT NULL default '0',
  `posts` int(11) NOT NULL default '0',
  `views` int(11) NOT NULL default '0',
  `important` enum('0','1') NOT NULL default '0',
  `description` TEXT NOT NULL,
  `group_access` varchar(255) default '' NOT NULL,
  `first_top` ENUM( '0', '1' ) DEFAULT '0' NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#####################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}users`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}users` (
	`id` int(6) NOT NULL auto_increment,
	`name` varchar(32) character set utf8 NOT NULL,
	`full_name` varchar( 255 ) character set utf8 NOT NULL,
	`passw` varchar(255) character set utf8 NOT NULL,
	`email` varchar(64) character set utf8 NOT NULL default '',
	`color` VARCHAR(7) character set utf8 NOT NULL default '',
	`state` VARCHAR(100) character set utf8 NOT NULL default '',
	`rating` INT DEFAULT '0' NOT NULL,
	`url` varchar(64) character set utf8 NOT NULL default '',
	`pol` ENUM( 'f', 'm', '' ) DEFAULT '' NOT NULL,
	`byear` INT( 4 ) DEFAULT 0 NOT NULL,
	`bmonth` INT( 2 ) DEFAULT 0 NOT NULL,
	`bday` INT( 2 ) DEFAULT 0 NOT NULL,
	`about` TEXT CHARACTER SET utf8 NOT NULL ,
	`signature` TEXT CHARACTER SET utf8 NOT NULL ,
	`photo` varchar(32) character set utf8 NOT NULL default '',
	`puttime` datetime NULL default NULL,
	`last_visit` datetime NULL default NULL,
	`themes` mediumint(8) unsigned NOT NULL default '0',
	`posts` int(10) unsigned NOT NULL default '0',
	`status` INT(2) NOT NULL default '1',
	`locked` tinyint(1) NOT NULL default '0',
	`activation` varchar(255) character set utf8 NOT NULL default '',
	`warnings` INT DEFAULT '0' NOT NULL,
	`ban_expire` DATETIME NULL DEFAULT NULL,
	`template` VARCHAR( 255 ) DEFAULT '' NOT NULL,
	`add_field_1` varchar(64) character set utf8,
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
##########################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}users_votes`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}users_votes` (
	`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
	`from_user` INT( 11 ) NOT NULL ,
	`to_user` INT( 11 ) NOT NULL ,
	`comment` TEXT CHARACTER SET utf8 NOT NULL ,
	`date` DATETIME NULL default NULL,
	`points` INT DEFAULT '0' NOT NULL,
	PRIMARY KEY ( `id` )
	) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci";
##########################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}foto`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}foto` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) character set utf8 NOT NULL,
  `description` TEXT character set utf8 NOT NULL,
  `filename` VARCHAR( 255 ) NOT NULL,
  `views` int(11) default '0',
  `date` datetime NULL default NULL,
  `category_id` VARCHAR( 255 ) NOT NULL,
  `author_id` int(11) NOT NULL,
  `comments` int(11) NOT NULL default '0',
  `commented` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
#############################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}foto_categories`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}foto_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) default '0',
  `announce` varchar(255) NOT NULL default '',
  `title` varchar(255) NOT NULL,
  `view_on_home` ENUM( '0', '1' ) DEFAULT '1' NOT NULL,
  `no_access` VARCHAR( 255 ) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}foto_categories` VALUES (1, 0, '', 'section', '1', '')";
#############################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}search_index`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}search_index` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`index` TEXT character set utf8 NOT NULL,
	`entity_id` INT(11) NOT NULL,
	`entity_table` VARCHAR(100) character set utf8 NOT NULL,
	`entity_view` VARCHAR(100) character set utf8 NOT NULL,
	`module` VARCHAR(100) character set utf8 NOT NULL,
	`date` DATETIME NULL default NULL,
	PRIMARY KEY (`id`),
	FULLTEXT KEY `index` (`index`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci";
#############################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}add_fields`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}add_fields` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`field_id` INT(11) NOT NULL,
	`module` VARCHAR(100) NOT NULL,
	`type` VARCHAR(10) NOT NULL,
	`label` VARCHAR(255) NOT NULL,
	`size` INT(11) NOT NULL,
	`params` VARCHAR(255) NOT NULL,
	`indexed` ENUM( '0', '1' ) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci";
$array[] = "INSERT INTO `{$_SESSION['prefix']}add_fields`
            (`id`, `field_id`, `module`, `type`, `label`, `size`, `params`, `indexed`) VALUES
            ('1', '1', 'users', 'text', 'jabber', '70', 'a:1:{s:7:\"pattern\";s:6:\"V_MAIL\";}', '0');";
#############################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}users_warnings`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}users_warnings` (
	`id` INT NOT NULL AUTO_INCREMENT ,
	`user_id` INT NOT NULL ,
	`admin_id` INT NOT NULL ,
	`cause` VARCHAR( 255 ) NOT NULL ,
	`date` DATETIME NULL default NULL,
	`points` INT DEFAULT '0' NOT NULL,
	PRIMARY KEY ( `id` )
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
#############################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}attaches`";
$array[] = "CREATE TABLE `{$_SESSION['prefix']}attaches` (
	`id` INT NOT NULL AUTO_INCREMENT ,
	`entity_id` INT NOT NULL,
	`user_id` INT NOT NULL ,
	`attach_number` INT NOT NULL ,
	`filename` VARCHAR( 100 ) NOT NULL ,
	`size` BIGINT NOT NULL ,
	`date` DATETIME NULL default NULL,
	`is_image` ENUM( '0', '1' ) DEFAULT '0' NOT NULL ,
	`module` varchar(10) default 'news' NOT NULL,
	PRIMARY KEY ( `id` )
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
#############################################################################
$array[] = "DROP TABLE IF EXISTS `{$_SESSION['prefix']}polls`";
$array[] = "CREATE TABLE IF NOT EXISTS `{$_SESSION['prefix']}polls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) NOT NULL,
  `variants` text NOT NULL,
  `voted_users` text NOT NULL,
  `question` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;";
#############################################################################




$n = 0;
foreach ($array as $key => $query) {
	$DB->query($query);
	if ($DB->getErrorInfo()) {
		$errors['query'] = 'При формировании базы данных произошел сбой! <br /> Начните  пожалуйста заново. (' . $query . ')';
		break;
	} else {
		echo '<span class="bd-zaprosy">' . $n . '. ' . htmlspecialchars(mb_substr($query, 0, 70, 'UTF-8')) . ' ...</span><br />';
		flush();
	}
	$n++;
}
if (empty($errors['query'])) {
    $DB->query("INSERT INTO `{$_SESSION['prefix']}users` (`id`, `name`, `passw`, `status`, `puttime`) 
    VALUES (1, '" . $_SESSION['adm_name'] . "', '" . password_hash($_SESSION['adm_pass'], PASSWORD_DEFAULT) . "', '4', NOW())");
    if ($DB->getErrorInfo())
        $errors['query'] = 'При формировании базы данных произошел сбой! <br /> Начните  пожалуйста заново.<br /><br />';
}

// Подстановка в robots.txt адреса сайта
createRobots();

if (empty($errors)) :
?>
<div class="finish">
    <h1 class="fin-h">База данных подготовлена</h1>
    <h2 class="fin-h">Выберите необходимые модули</h2>
</div>
<?php 
 
else: 
	echo '<div style="position:absolute;top:300px;left:35%;width:400px;">' . $errors['query'] . '</div>';
	//if (mysql_error()) echo mysql_error();
endif;


?>
