# Credits

Atom-M CMS is based on:

* AtomX CMS https://github.com/Drunyacoder/AtomXCMS-2
* jQuery https://github.com/jquery/jquery
* Materialize https://github.com/Dogfalo/materialize
* Highcharts JS https://github.com/highslide-software/highcharts.com
* CodeMirror https://github.com/codemirror/codemirror
* Twig http://twig.sensiolabs.org/
* phpFileManager http://phpfm.sf.net
* password_compat https://github.com/ircmaxell/password_compat
* HTMLPurifier http://htmlpurifier.org/

Atom-M CMS is mainly developed by Alexander (@modos189) Danilov and Boris (@MrBoriska) Lapin under the MIT License

Thank you so much to all contributors.
